<?php

class Bio_model extends CI_Model
{


    //######################Cricketers Bio##############################

//    Show Publish Cricketers categories
    Public function show_publish_cricketers_category()
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $this->db->where('main_category_id', 1 AND 'status', 1);
        $query_result = $this->db->get();
        $category_info = $query_result->result();
        return $category_info;
    }

    // Save Cricketers Bio
    public function save_cricketers_bio_info()
    {
        $data = array();

        $data['celebrity_id'] = $this->input->post('cricketers_name', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['intro'] = $this->input->post('intro', TRUE);
        $data['early_years'] = $this->input->post('early_years', TRUE);
        $data['personal_life'] = $this->input->post('personal_life', TRUE);
        $data['domestic_cricket'] = $this->input->post('domestic_cricket', TRUE);
        $data['international_career'] = $this->input->post('international_career', TRUE);
        $data['achivment'] = $this->input->post('achivment', TRUE);
        $data['awards'] = $this->input->post('awards', TRUE);

        $config['upload_path'] = './upload/cricketers/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 100000;
//        $config['max_width'] = 10024;
//        $config['max_height'] = 7608;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('upload_form', $error);
        } else {
            $uploadData = $this->upload->data();

            //Resize
            $this->cricketers_bio_image_resize('./upload/cricketers/' .$uploadData['file_name'], $uploadData['file_name']);

            $data['img'] = $uploadData['file_name'];

        }



        $data['status'] = $this->input->post('status', TRUE);
        $data['meta_tag'] = $this->input->post('meta_tag', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);



//        =============Inserted To Table=================
        $this->db->insert('cricketers_bio', $data);


    }

    //#Image Resize
    public function cricketers_bio_image_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] =  FALSE;
        $config_resize['maintain_ratio'] =  TRUE;
        $config_resize['quality'] =  '80%';
        $config_resize['height']          =  270;
        $config_resize['new_image']          = './upload/cricketers/' . $file;
        $config_resize['wm_type'] = 'overlay';
        $config_resize['wm_overlay_path'] = './upload/logo.png';
        $config_resize['wm_opacity'] = '30';
        $config_resize['wm_x_transp'] = '4';
        $config_resize['wm_y_transp'] = '4';
        $config_resize['wm_vrt_alignment'] = 'middle';
        $config_resize['wm_hor_alignment'] = 'right';
        $config_resize['wm_padding'] = '1';
        $this->image_lib->initialize($config_resize);
        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
        $this->image_lib->watermark();
    }


//    Manage Cricketers Bio
    public function manage_cricketers_bio_info()
    {
        $this->db->select('*');
        $this->db->from('cricketers_bio');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }


//   view_cricketers_for_home
    public function view_cricketers_for_home()
    {
        $this->db->select('*');
        $this->db->from('cricketers_bio');
        $this->db->where('status', '1');
        $this->db->order_by('rand()');
        $this->db->limit('5');
        $this->db->order_by("id","desc");
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }

//    Find Category
    public function find_celebrities_category($id)
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $this->db->where('id', $id);
        $result_query = $this->db->get();
        $category_name = $result_query->row();

        return $category_name;
    }

    // show_for_update_cricketers_bio_info
    public function show_for_update_cricketers_bio_info($id)
    {
        $this->db->select('*');
        $this->db->from('cricketers_bio');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $cricketers_info = $query_result->row();
        return $cricketers_info;
    }




    // show_cricketers_details_bio_info
    public function show_cricketers_details_bio_info($id)
    {
        $this->db->select('*');
        $this->db->from('cricketers_bio');
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->order_by('created_at', 'DESC');
        $query_result = $this->db->get();
        $cricketers_details = $query_result->row();
        return $cricketers_details;
    }

//selected_publish_category
    public function selected_celebrities_publish_category()
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }

    // Update Cricketers Bio
    public function update_cricketers_bio_info()
    {
        $data = array();

        $data['id'] = $this->input->post('id', TRUE);
        $data['celebrity_id'] = $this->input->post('celebrity_id', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['intro'] = $this->input->post('intro', TRUE);
        $data['early_years'] = $this->input->post('early_years', TRUE);
        $data['personal_life'] = $this->input->post('personal_life', TRUE);
        $data['domestic_cricket'] = $this->input->post('domestic_cricket', TRUE);
        $data['international_career'] = $this->input->post('international_career', TRUE);
        $data['achivment'] = $this->input->post('achivment', TRUE);
        $data['awards'] = $this->input->post('awards', TRUE);

        if ($_FILES['file']['name'] != "") {
            $config['upload_path'] = './upload/cricketers/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['raw_name'] = 'cricketers';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('upload_form', $error);
            } else {
                $uploadData = $this->upload->data();

                //Resize
                $this->cricketers_bio_image_resize('./upload/cricketers/' .$uploadData['file_name'], $uploadData['file_name']);

                $data['img'] = $uploadData['file_name'];
            }
        } else {
            $data['img'] = $this->input->post('old');
        }

        $data['status'] = $this->input->post('status', TRUE);
        $data['meta_tag'] = $this->input->post('meta_tag', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);

        $this->db->where('id', $data['id']);
        $this->db->update('cricketers_bio', $data);
    }


    // delete Cricketers Bio
    public function delete_cricketers_bio_info($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('cricketers_bio');
    }

//un_publish_cricketers_bio_info
    public function un_publish_cricketers_bio_info($id)
    {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update('cricketers_bio');
    }

//publish_cricketers_bio_info
    public function publish_cricketers_bio_info($id)
    {
        $this->db->set('status', 1);
        $this->db->where('id', $id);
        $this->db->update('cricketers_bio');
    }

    //######################Cricketers Bio END##############################


    //######################==Actress Bio Start==##############################

    //    Show Publish Actress categories
    Public function show_publish_actress_category()
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $this->db->where('main_category_id', 3);
        $this->db->where('status', 1);
        $this->db->order_by('id', DESC);
        $query_result = $this->db->get();
        $actress_category_info = $query_result->result();
        return $actress_category_info;
    }

    // Save actress Bio
    public function save_actress_bio_info()
    {
        $data = array();

        $data['celebrity_id'] = $this->input->post('actress_category', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['intro'] = $this->input->post('intro', TRUE);
        $data['early_life'] = $this->input->post('early_life', TRUE);
        $data['personal_life'] = $this->input->post('personal_life', TRUE);
        $data['career'] = $this->input->post('career', TRUE);
        $data['filmography'] = $this->input->post('filmography', TRUE);
        $data['awards'] = $this->input->post('awards', TRUE);

        $config['upload_path'] = './upload/actress/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 500000;
        $config['max_width'] = 10024;
        $config['max_height'] = 7608;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('upload_form', $error);
        } else {
            $uploadData = $this->upload->data();

            //Resize
            $this->actress_bio_image_resize('./upload/actress/' .$uploadData['file_name'], $uploadData['file_name']);

            $data['img'] = $uploadData['file_name'];
        }

        $data['status'] = $this->input->post('status', TRUE);
        $data['meta_tag'] = $this->input->post('meta_tag', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);

//        =============Inserted To Table=================
        $this->db->insert('actors_bio', $data);
    }

    //    Manage Actress Bio
    public function manage_actress_bio_info()
    {
        $this->db->select('*');
        $this->db->from('actors_bio');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }

    //    Manage Actress Bio For Home Page Aside of Slider
    public function view_actress_bio_for_home()
    {
        $this->db->select('*');
        $this->db->from('actors_bio');
        $this->db->where('status', '1');
        $this->db->order_by('rand()');
        $this->db->limit('4');
        $this->db->order_by("id","desc");
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }




    // show_for_update_actress_bio_info
    public function show_for_update_actress_bio_info($id)
    {
        $this->db->select('*');
        $this->db->from('actors_bio');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $actress_info = $query_result->row();
        return $actress_info;
    }


    // show_actress_details_bio_info
    public function show_actress_details_bio_info($id)
    {
        $this->db->select('*');
        $this->db->from('actors_bio');
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->order_by('created_at', 'desc');
        $query_result = $this->db->get();
        $actress_details = $query_result->row();
        return $actress_details;
    }

//selected_publish_actress_category
    public function selected_actress_publish_category()
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }

    // Update actress Bio
    public function update_actress_bio_info()
    {
        $data = array();

        $data['id'] = $this->input->post('id', TRUE);
        $data['celebrity_id'] = $this->input->post('category_name', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['intro'] = $this->input->post('intro', TRUE);
        $data['early_life'] = $this->input->post('early_life', TRUE);
        $data['personal_life'] = $this->input->post('personal_life', TRUE);
        $data['career'] = $this->input->post('career', TRUE);
        $data['filmography'] = $this->input->post('filmography', TRUE);
        $data['awards'] = $this->input->post('awards', TRUE);

        if ($_FILES['file']['name'] != "") {
            $config['upload_path'] = './upload/actress/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('upload_form', $error);
            } else {
                $uploadData = $this->upload->data();

                //Resize
                $this->actress_bio_image_resize('./upload/actress/' .$uploadData['file_name'], $uploadData['file_name']);

                $data['img'] = $uploadData['file_name'];
            }
        } else {
            $data['img'] = $this->input->post('old');
        }

        $data['status'] = $this->input->post('status', TRUE);
        $data['meta_tag'] = $this->input->post('meta_tag', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);

        $this->db->where('id', $data['id']);
        $this->db->update('actors_bio', $data);
    }


    // delete actress Bio
    public function delete_actress_bio_info($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('actors_bio');
    }

//un_publish_actress_bio_info
    public function un_publish_actress_bio_info($id)
    {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update('actors_bio');
    }

//publish_actress_bio_info
    public function publish_actress_bio_info($id)
    {
        $this->db->set('status', 1);
        $this->db->where('id', $id);
        $this->db->update('actors_bio');
    }






    //#Image Resize
    public function actress_bio_image_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] =  FALSE;
        $config_resize['maintain_ratio'] =  TRUE;
        $config_resize['quality'] =  '100%';
        $config_resize['height']          =  270;
        $config_resize['new_image']          = './upload/actress/' . $file;
        $this->image_lib->initialize($config_resize);
        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
    }
    
    
    
    
    
    //  public function actress_bio_image_resize($path, $file) {
    //     $config_resize = array();
    //     $config_resize['image_library'] = 'gd2';
    //     $config_resize['source_image'] = $path;
    //     $config_resize['create_thumb'] =  FALSE;
    //     $config_resize['maintain_ratio'] =  TRUE;
    //     $config_resize['quality'] =  '100%';
    //     $config_resize['height']          =  270;
    //     $config_resize['new_image']          = './upload/actress/' . $file;
    //     $config_resize['wm_type'] = 'overlay';
    //     $config_resize['wm_overlay_path'] = './upload/logo.png';
    //     $config_resize['wm_opacity'] = '30';
    //     $config_resize['wm_x_transp'] = '4';
    //     $config_resize['wm_y_transp'] = '4';
    //     $config_resize['wm_vrt_alignment'] = 'middle';
    //     $config_resize['wm_hor_alignment'] = 'right';
    //     $config_resize['wm_padding'] = '1';
    //     $this->image_lib->initialize($config_resize);
    //     $this->load->library('image_lib', $config_resize);
    //     $this->image_lib->resize();
    //     $this->image_lib->watermark();
    // }
//######################Actress Bio END##############################




    //######################==singers Bio Start==##############################

    //    Show Publish singers categories
    Public function show_publish_singers_category()
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $this->db->where('main_category_id', 2);
        $this->db->where('status', 1);
        $query_result = $this->db->get();
        $singers_category_info = $query_result->result();
        return $singers_category_info;
    }

    // Save singers Bio
    public function save_singers_bio_info()
    {
        $data = array();

        $data['celebrity_id'] = $this->input->post('celebrity_id', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['intro'] = $this->input->post('intro', TRUE);
        $data['early_life'] = $this->input->post('early_life', TRUE);
        $data['personal_life'] = $this->input->post('personal_life', TRUE);
        $data['career'] = $this->input->post('career', TRUE);
        $data['discography'] = $this->input->post('discography', TRUE);
        $data['awards'] = $this->input->post('awards', TRUE);

        $config['upload_path'] = './upload/singers/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 500000;
        $config['max_width'] = 10024;
        $config['max_height'] = 7608;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $error = array('error' => $this->upload->display_errors());

            $this->load->view('upload_form', $error);
        } else {
            $uploadData = $this->upload->data();

            //Resize
            $this->singers_bio_image_resize('./upload/singers/' .$uploadData['file_name'], $uploadData['file_name']);

            $data['img'] = $uploadData['file_name'];
        }

        $data['status'] = $this->input->post('status', TRUE);
        $data['meta_tag'] = $this->input->post('meta_tag', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);

//        =============Inserted To Table=================
        $this->db->insert('singers_bio', $data);
    }

    //    Manage singers Bio
    public function manage_singers_bio_info()
    {
        $this->db->select('*');
        $this->db->from('singers_bio');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }
    //    Manage singers Bio
    public function view_singers_for_home()
    {
        $this->db->select('*');
        $this->db->from('singers_bio');
        $this->db->where('status', '1');
        $this->db->order_by('rand()');
        $this->db->limit('4');
        $this->db->order_by("id","DESC");
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }




    // show_for_update_singers_bio_info
    public function show_for_update_singers_bio_info($id)
    {
        $this->db->select('*');
        $this->db->from('singers_bio');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $singers_info = $query_result->row();
        return $singers_info;
    }



    // show_singers_details_bio_info
    public function show_singers_details_bio_info($id)
    {
        $this->db->select('*');
        $this->db->from('singers_bio');
        $this->db->where('id', $id);
        $this->db->where('status', 1);
        $this->db->order_by('created_at', 'desc');
        $query_result = $this->db->get();
        $singer_details = $query_result->row();
        return $singer_details;
    }



//selected_publish_singers_category
    public function selected_singers_publish_category()
    {
        $this->db->select('*');
        $this->db->from('celebrity_categories');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }

    // Update singers Bio
    public function update_singers_bio_info()
    {
        $data = array();

        $data['id'] = $this->input->post('id', TRUE);
        $data['celebrity_id'] = $this->input->post('celebrity_id', TRUE);
        $data['title'] = $this->input->post('title', TRUE);
        $data['name'] = $this->input->post('name', TRUE);
        $data['intro'] = $this->input->post('intro', TRUE);
        $data['early_life'] = $this->input->post('early_life', TRUE);
        $data['personal_life'] = $this->input->post('personal_life', TRUE);
        $data['career'] = $this->input->post('career', TRUE);
        $data['discography'] = $this->input->post('discography', TRUE);
        $data['awards'] = $this->input->post('awards', TRUE);

        if ($_FILES['file']['name'] != "") {
            $config['upload_path'] = './upload/singers/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('upload_form', $error);
            } else {
                $uploadData = $this->upload->data();

                //Resize
                $this->singers_bio_image_resize('./upload/singers/' .$uploadData['file_name'], $uploadData['file_name']);

                $data['img'] = $uploadData['file_name'];
            }
        } else {
            $data['img'] = $this->input->post('old');
        }

        $data['status'] = $this->input->post('status', TRUE);
        $data['meta_tag'] = $this->input->post('meta_tag', TRUE);
        $data['meta_description'] = $this->input->post('meta_description', TRUE);

        $this->db->where('id', $data['id']);
        $this->db->update('singers_bio', $data);
    }


    // delete singers Bio
    public function delete_singers_bio_info($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('singers_bio');
    }

//un_publish_singers_bio_info
    public function un_publish_singers_bio_info($id)
    {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update('singers_bio');
    }

//publish_singers_bio_info
    public function publish_singers_bio_info($id)
    {
        $this->db->set('status', 1);
        $this->db->where('id', $id);
        $this->db->update('singers_bio');
    }


    //#Image Resize
   public function singers_bio_image_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] =  FALSE;
        $config_resize['maintain_ratio'] =  TRUE;
        $config_resize['quality'] =  '90%';
        $config_resize['height']          =  270;
        $config_resize['new_image']          = './upload/singers/' . $file;
        $this->image_lib->initialize($config_resize);
        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
    }
//######################singers Bio END##############################

}