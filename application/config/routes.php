<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['about'] = 'Home/editor';
$route['contact'] = 'Home/contact';
$route['popular-bangladeshi-cricketers'] = 'Home/cricketers';
$route['popular-bangladeshi-actress'] = 'Home/actors';
$route['popular-bangladeshi-singers'] = 'Home/singers';

//Bio
$route['top-bangladeshi-singers-bio'] = 'Home/singers_bio';
$route['top-bangladeshi-actress-bio'] = 'Home/actress_bio';
$route['top-bangladeshi-cricketers-bio'] = 'Home/cricketers_bio';

$route['copyright'] = 'Home/copyright';
$route['privacy-policy'] = 'Home/privacy_policy';
$route['about'] = 'Home/about';







//User/Admin
$route['admin/change-password'] = 'Super_admin/changePass';


$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Main Categories
$route['publish-category/(.+)'] = 'Category/publish_category/$1';
$route['un_publish-category/(.+)'] = 'Category/un_publish_category/$1';
$route['delete-category/(.+)'] = 'Category/delete_category/$1';
$route['update-category/(.+)'] = 'Category/show_for_update_category/$1';
$route['view-all-category'] = 'Category/manage_category/';


//Celebrities_Categories
$route['add-celebrity-category'] = 'Category/add_celebrity_category/';
$route['view-celebrity-category'] = 'Category/manage_celebrity_category/';
$route['publish-celebrity-category/(.+)'] = 'Category/publish_celebrity_category/$1';
$route['un_publish-celebrity-category/(.+)'] = 'Category/un_publish_celebrity_category/$1';
$route['delete-celebrity-category/(.+)'] = 'Category/delete_celebrity_category/$1';
$route['update-celebrity-category/(.+)'] = 'Category/show_for_update_celebrity_category/$1';


// ==================Bio====================

//#Cricketers Bio
$route['create-cricketers-bio'] = 'Bio/add_cricketers_bio/';
$route['view-cricketers-bio'] = 'Bio/manage_cricketers_bio/';
$route['update-cricketers-bio/(.+)'] = 'Bio/show_for_update_cricketers_bio/$1';
$route['delete-cricketers-bio/(.+)'] = 'Bio/delete_cricketers_bio/$1';
$route['un-publish-cricketers-bio/(.+)'] = 'Bio/un_publish_cricketers_bio/$1';
$route['publish-cricketers-bio/(.+)'] = 'Bio/publish_cricketers_bio/$1';



//#Actress Bio
$route['create-actress-bio'] = 'Bio/add_actress_bio/';
$route['view-actress-bio'] = 'Bio/manage_actress_bio/';
$route['update-actress-bio/(.+)'] = 'Bio/show_for_update_actress_bio/$1';
$route['delete-actress-bio/(.+)'] = 'Bio/delete_actress_bio/$1';
$route['un-publish-actress-bio/(.+)'] = 'Bio/un_publish_actress_bio/$1';
$route['publish-actress-bio/(.+)'] = 'Bio/publish_actress_bio/$1';


//#======================details Page View=========================
$route['actress/(.+.+)'] = 'Home/show_actress_details_bio/$1/$2';
$route['cricketer/(.+.+)'] = 'Home/show_cricketers_details_bio/$1/$2';
$route['singer/(.+.+)'] = 'Home/show_singers_details_bio/$1/$2';



//#Singers Bio
$route['create-singers-bio'] = 'Bio/add_singers_bio/';
$route['view-singers-bio'] = 'Bio/manage_singers_bio/';
$route['update-singers-bio/(.+)'] = 'Bio/show_for_update_singers_bio/$1';
$route['delete-singers-bio/(.+)'] = 'Bio/delete_singers_bio/$1';
$route['un-publish-singers-bio/(.+)'] = 'Bio/un_publish_singers_bio/$1';
$route['publish-singers-bio/(.+)'] = 'Bio/publish_singers_bio/$1';


//# Main Slider
$route['add-main-slider'] = 'Common_images/add_main_slider';
$route['view-main-slider'] = 'Common_images/manage_main_slider';
$route['un-publish-main-slider/(.+)'] = 'Common_images/un_publish_main_slider/$1';
$route['publish-main-slider/(.+)'] = 'Common_images/publish_main_slider/$1';
$route['delete-main-slider/(.+)'] = 'Common_images/delete_main_slider/$1';
$route['update-main-slider/(.+)'] = 'Common_images/show_for_update_main_slider/$1';


//# XML
$route['seo/sitemap\.xml'] = "Seo/sitemap";





