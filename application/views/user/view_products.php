<?php
include './component/header.php';
include './component/sidebar.php';
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

$products = $model->view_all_by_cond('image', 'category_id >= 7 AND category_id <= 9');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">

            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;">Products  View</h2>
            </div>

            <!---=======================Data Table=====================------>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Category</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Popular Product</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($products as $product) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <?php
                                        $viewCategory = $model->details_by_cond('categories
', 'id = ' . $product['category_id'] . '');
                                        echo $viewCategory['category_name'];
                                        ?>
                                    </td>
                                    <td><?php echo $product['title']; ?></td>
                                    <td><?php echo $product['description']; ?></td>
                                    <td>
                                        <img style="width:50px; height:auto;"
                                             src="../assets/img/products/<?php echo isset($product['img']) ? $product['img'] : null ?>">
                                    </td>
                                    <td><?php  if($product['popular_product'] == 1){
                                        echo 'YES';
                                    }else{
                                        echo 'Null';
                                    } ?></td>

                                    <td>
                                        <a href="edit_product.php?product_id=<?php echo $product['id']; ?>">
                                            <button type="button" class="btn btn-success">Edit</button>
                                        </a>
                                        <a onclick="return confirm('Are you sure you want to delete?')" href="deleteProduct.php?product_id=<?php echo $product['id']; ?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


        </div>
        <!-- /.row -->


    </div>

</div>
<!-- /.content-wrapper -->

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<?php
include './component/footer.php';
?>
