<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

include './component/header.php';
include './component/sidebar.php';


if (isset($_POST['submit'])) {
    $error = $slider->insertWelcome($_POST);
}
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="box-body">

            <div class="row">
                <div>
                    <h2 class="bg-success text-primary text-center"
                        style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Insert Welcome Message'; ?> </h2>
                </div>
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="col-md-8 col-md-offset-2">


                        <!--Welcome Message-->
                        <label>Welcome Message : </label>
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body pad">

                                    <textarea name="message" class="wysihtml5"
                                              placeholder="Place some text here"
                                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                            </div>
                        </div>


                        <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                    </div>

                </form>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.content-wrapper -->

<?php
include './component/footer.php';
?>