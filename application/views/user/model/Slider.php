<?php

include_once("Database.php");
include_once("CommonClass.php");
$model = new CommonClass();

class Slider extends Database
{

    public $con;

    public function __construct()
    {

        $this->con = parent::connect();
    }

    public function insertSlider($postData, $fileData)
    {


        $prod_title = $postData['slider_title'];
        $prod_title = mysqli_real_escape_string($this->con, $prod_title);

        $slider_description = $postData['textarea'];
        $slider_description = mysqli_real_escape_string($this->con, $slider_description);


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/img/slider_image/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);


        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `title`, `description`, `img`) VALUES (NULL, '1', '$prod_title', '$slider_description', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }


    public function insertServices($postData, $fileData)
    {


        $prod_title = $postData['slider_title'];
        $prod_title = mysqli_real_escape_string($this->con, $prod_title);

        $slider_description = $postData['textarea'];
        $slider_description = mysqli_real_escape_string($this->con, $slider_description);


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/img/services/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);


        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `title`, `description`, `img`) VALUES (NULL, '10', '$prod_title', '$slider_description', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }

    public function showSliderImages()
    {

        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `image` WHERE `category_id` = 1  ORDER BY `image`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 4) {
                break;
            }
            $returnData[] = $row;
            $i++;
        }
        return $returnData;
    }

    public function showForEdit()
    {

        $sql_query = 'SELECT * FROM `slider_table` WHERE `category` = 1  ORDER BY `slider_table`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }

        return $returnData;
    }

    public function showSliderCategory()
    {

        $returnData = [];
        $sql_query_for_category = 'SELECT * FROM `slider_table`';
        $queryExecute = mysqli_query($this->con, $sql_query_for_category);

        while ($row_for_category = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $queryForCategoryName = "SELECT * FROM `category_id_name` WHERE `catagory_id` = " . $row_for_category['category'] . "";
            $CategoryName = mysqli_query($this->con, $queryForCategoryName);
            $showCategoryName = mysqli_fetch_array($CategoryName, MYSQLI_ASSOC);

            $returnData = $showCategoryName;
        }
        return $returnData;
    }

    public function showCategory()
    {
        $returnData = [];
        $sql_query_for_category = 'SELECT * FROM `category_id_name`';
        $queryExecute = mysqli_query($this->con, $sql_query_for_category);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }
        return $returnData;
    }

    public function showSingleSlider($id)
    {

        $queryForSingleSlider = "SELECT * FROM `slider_table` WHERE `id` = $id";
        $SingleSlider = mysqli_query($this->con, $queryForSingleSlider);
        $returnData = mysqli_fetch_array($SingleSlider, MYSQLI_ASSOC);

        return $returnData;
    }

    public function updateSlider($postData, $fileData)
    {


        $prod_catagory = $postData['prod_catogory'];
        $prod_title = $postData['add_title'];
        $id = $postData['id'];
        $prod_title = mysqli_real_escape_string($this->con, $prod_title);

        $singleSlider = $this->showSingleSlider($id);

        if ($prod_title == "") {
            $error = "Please input Image Title";
        }
        if ($prod_catagory == "") {
            $error = "Please input Product Category";
        } else if ($fileData['file']['size'] > 250000) {
            $error = "Please insert image not more than 250 KB";
        }

        if (isset($fileData['file']['name']) && !empty($fileData['file']['name'])) {
            $file_name = uniqid() . $fileData['file']['name'];
            $destination = "../img/all-images/" . $file_name;
            $upload = move_uploaded_file($fileData['file']['tmp_name'], $destination);
        } else {
            $file_name = $singleSlider['img_link'];
        }

        $product_update_query = "UPDATE `slider_table` SET `category` = '$prod_catagory', `title` = '$prod_title', `img_link` = '$file_name' WHERE `slider_table`.`id` = $id";

        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());
        if ($update_query) {
            ?>
            <script>
                window.location = "viewSlider.php";
            </script>
            <?php
        } else {
            $error = "Error! Could not Upload your Information";

        }
        return $update_query;
    }


    //insert insertProducts
    public function insertProducts($postData, $fileData)
    {


        $category = $postData['category'];

        
        $productTitle = $postData['productTitle'];
        $productTitle = mysqli_real_escape_string($this->con, $productTitle);
        
        $productDescription = $postData['productDescription'];
        $productDescription = mysqli_real_escape_string($this->con, $productDescription);
        
        $popularStatus = $postData['popularStatus'];

        

        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/img/products/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        
        
        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `title`, `description`, `img`, `popular_product`) VALUES (NULL, '$category', '$productTitle', '$productDescription', '$file_name', '$popularStatus')";


        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }




    //insertWelcome
    public function insertWelcome($postData)
    {

        $welcomeMessage = $postData['message'];
        $welcomeMessage = mysqli_real_escape_string($this->con, $welcomeMessage);




        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `description`) VALUES (NULL, '2', '$welcomeMessage')";


        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! Upload your Welcome Message ";
        } else {
            return "Error! Could not Upload your  Welcome Message";
        }
    }






    // insert  About
    public function insertAbout($postData, $fileData)
    {

        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/img/about/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `description`, `img`) VALUES (NULL, '4', '$description', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your About information ";
        } else {
            return "Error! Could not Upload your About Information";
        }
    }






    //insertCategory
    public function insertCategory($postData)
    {

        $name = $postData['name'];
        $name = mysqli_real_escape_string($this->con, $name);


        $product_insert_query = "INSERT INTO `categories` (`id`, `category_name`) VALUES (NULL, '$name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query) {

            return "Success! ADD your New Category ";
        } else {
            return "Error! Could not Add your New Category";
        }
    }





    // insert gallery
    public function insertGallery( $fileData)
    {


        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../assets/img/gallery_img/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        $product_insert_query = "INSERT INTO `image` (`id`, `category_id`, `img`) VALUES (NULL, '6', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);

        if ($insert_query && $upload) {

            return "Success! Upload your Gallery  Image ";
        } else {
            return "Error! Could not Upload your Gallery  Image ";
        }
    }



//============================================INSERT END==================================//




//===================================Update Start================================//

//Update About
    public function updateAbout($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/img/about/');
        } else {
            $file_name = '';
        }

        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '4',
            'description' => $description,
            'img' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }


        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_about.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//Update Welcome
    public function updateWelcome($postData)
    {
        global $model;
        $id = $postData['id'];

        $description = $postData['description'];
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => 2,
            'description' => $description
        );



        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_welcome.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//updateSecurityServices
    public function updateProducts($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/img/products/');
        } else {
            $file_name = '';
        }

        $popularStatus = $postData['popularStatus'];


        $title = $postData['title'];

        $title = mysqli_real_escape_string($this->con, $title);


        $description = $postData['description'];

        $description = mysqli_real_escape_string($this->con, $description);
        $formData = array(
            'category_id' => $postData['category'],
            'title' => $title,
            'description' => $description,
            'img' => $file_name,
            'popular_product' => $popularStatus

        );


        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_products.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//updateClientInfo
    public function updateClientInfo($postData)
    {
        global $model;
        $id = $postData['id'];

        $clientName = $postData['clientName'];
        $clientName = mysqli_real_escape_string($this->con, $clientName);

        $projectName = $postData['projectName'];
        $projectName = mysqli_real_escape_string($this->con, $projectName);

        $remarks = $postData['remarks'];
        $remarks = mysqli_real_escape_string($this->con, $remarks);

        $formData = array(
            'client_name' => $clientName,
            'project_name' => $projectName,
            'remarks' => $remarks
        );


        $update_query = $model->Update_data('client', $formData, "`client`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_clientInformation.php";
            </script>
            <?php

            return "Success! Client Information Updated succesfully ";
        } else {
            return "Error! Could not Update your Client Information";
        }
    }



//updateCareers
    public function updateCareers($postData)
    {
        global $model;
        $id = $postData['id'];

        $shortDescription = $postData['shortDescription'];
        $shortDescription = mysqli_real_escape_string($this->con, $shortDescription);

        $importantFacts = $postData['importantFacts'];
        $importantFacts = mysqli_real_escape_string($this->con, $importantFacts);


        $job_title = $postData['job_title'];
        $job_title = mysqli_real_escape_string($this->con, $job_title);


        $responsibility = $postData['responsibility'];
        $responsibility = mysqli_real_escape_string($this->con, $responsibility);


        $job_type = $postData['job_type'];
        $job_type = mysqli_real_escape_string($this->con, $job_type);


        $salary_range = $postData['salary_range'];
        $salary_range = mysqli_real_escape_string($this->con, $salary_range);


        $Qualification = $postData['Qualification'];
        $Qualification = mysqli_real_escape_string($this->con, $Qualification);


        $location = $postData['location'];
        $location = mysqli_real_escape_string($this->con, $location);

        $formData = array(
            'short_description' => $shortDescription,
            'important_facts' => $importantFacts,
            'job_title' => $job_title,
            'responsibility' => $responsibility,
            'job_type' => $job_type,
            'salary_range' => $salary_range,
            'qualification' => $Qualification,
            'location' => $location
        );


        $update_query = $model->Update_data('carees', $formData, "`carees`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_careers.php";
            </script>
            <?php

            return "Success! Career Information Updated succesfully ";
        } else {
            return "Error! Could not Update your Career Information";
        }
    }



//    Slider Update
    public function updateSliderInfo($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/img/slider_image/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '1',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "product_view.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }





//    Services Update
    public function updateServices($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/img/services/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '10',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_services.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }



//    updateOurServices
    public function updateOurServices($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/services_img/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '2',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_services.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateSocialResponsibility
    public function updateSocialResponsibility($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/socialResponsibility/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '3',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_responsibility.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateTestimonial
    public function updateTestimonial($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/testimonial/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '4',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_testimonial.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }


//    updateNews
    public function updateNews($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/news/');
        } else {
            $file_name = '';
        }

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '5',
            'title' => $services_title,
            'description' => $description,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_news.php";
            </script>
            <?php

            return "Success! News Data Updated succesfully ";
        } else {
            return "Error! Could not Update your News Data";
        }
    }


//    updateWhyChooseUs
    public function updateWhyChooseUs($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '6',
            'title' => $services_title,
            'description' => $description
        );


        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_whyChooseUs.php";
            </script>
            <?php

            return "Success! News Data Updated succesfully ";
        } else {
            return "Error! Could not Update your News Data";
        }
    }


//    updateSpecification
    public function updateSpecification($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];

        $services_title = $postData['slider_title'];
        $description = $postData['description'];

        $services_title = mysqli_real_escape_string($this->con, $services_title);
        $description = mysqli_real_escape_string($this->con, $description);

        $formData = array(
            'category_id' => '7',
            'title' => $services_title,
            'description' => $description
        );


        $update_query = $model->Update_data('text', $formData, "`text`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_specification.php";
            </script>
            <?php

            return "Success! News Data Updated succesfully ";
        } else {
            return "Error! Could not Update your News Data";
        }
    }



//Update Gallery
    public function updateGallery($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../assets/img/gallery_img/');
        } else {
            $file_name = '';
        }


        $formData = array(
            'category_id' => '6',
            'img' => $file_name
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_gallery.php";
            </script>
            <?php

            return "Success! Data Updated successfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }



//    Update Partner
    public function updatePartner($postData, $fileData)
    {
        global $model;
        $id = $postData['id'];
        $fileExist = FALSE;

        if (!empty($fileData['file']['name'])) {
            $fileExist = TRUE;
            $file_name = $model->file_upload($fileData, '../img/partner/');
        } else {
            $file_name = '';
        }

        $gallery_title = $postData['title'];

        $gallery_title = mysqli_real_escape_string($this->con, $gallery_title);

        $formData = array(
            'category_id' => '9',
            'title' => $gallery_title,
            'img' => $file_name,
        );

        if ($fileExist == FALSE) {
            unset($formData['img']);
        }

        $update_query = $model->Update_data('image', $formData, "`image`.`id` = $id");

        if ($update_query) {
            ?>
            <script>
                window.location = "view_partner.php";
            </script>
            <?php

            return "Success! Data Updated succesfully ";
        } else {
            return "Error! Could not Update your Data";
        }
    }

}
