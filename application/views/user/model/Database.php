<?php

class Database {

    public $server = 'localhost';
    public $userName = 'root';
    public $password = '';
    public $dbName = 'jahan_dept_info';

    public function connect() {
        $con = mysqli_connect($this->server, $this->userName, $this->password, $this->dbName);
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        
        return $con;
    }

}

?>