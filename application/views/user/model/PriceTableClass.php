<?php

include_once("Database.php");

class PriceTableClass extends Database{

    public $con;

    public function __construct() {
        
        $this->con = parent::connect();
    }

    public function insert($postData) {

        $nameOfPackage = ($_POST['add_nameOfPackage']);
        $packageTitle = ($_POST['add_package_title']);
        $price = ($_POST['add_price']);
        $detailsSerialize = array(
            $_POST['details_speed'],
            $_POST['details_direct_speed'],
            $_POST['details_youtube'],
            $_POST['details_ftp'],
            );
        $details = serialize($detailsSerialize); 

        $nameOfPackage = mysqli_real_escape_string($this->con, $nameOfPackage);
        $price = mysqli_real_escape_string($this->con, $price);
        $details = mysqli_real_escape_string($this->con, $details);

        $product_insert_query = "INSERT INTO `pricing_table` (`id`, `features`, `package_name`, `package_title`, `package_price`, `package_description`) VALUES (NULL, '0', '$nameOfPackage', '$packageTitle', '$price', '$details')";
        
        $insert_query = mysqli_query($this->con, $product_insert_query);
        if ($insert_query) {
            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }
    
    public function showFeaturedPriceTable(){
        
        $returnData = [];
        $i = 0 ;
        $sql_query = 'SELECT * FROM `pricing_table`  ORDER BY `pricing_table`.`id` ASC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            if ($i > 2) {
                break;
            }
            $returnData[] = $row;
            $i ++;
        }
        return $returnData;
    }
    
    public function showAllPriceTable(){
        
        $returnData = [];
        $sql_query = 'SELECT * FROM `pricing_table`';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            $returnData[] = $row;
        }
        return $returnData;
    }
    
    
    public function makeFeatured() {

        $sql_query = 'SELECT * FROM `pricing_table` WHERE `features` = 1';
        $rowcount = mysqli_num_rows($sql_query);
        if ($rowcount <= 3) {

            $queryMakeFeatured = "UPDATE `pricing_table` SET `features` = '1'";
            $insert_query = mysqli_query($this->con, $queryMakeFeatured) or die(mysql_error());
        } else {
            return 'Sorry alread 3 table is featured!';
        }
    }
    public function showSingleTable($id) {

        $queryForSingleTable = "SELECT * FROM `pricing_table` WHERE `id` = $id";
        $SingleTable = mysqli_query($this->con, $queryForSingleTable);
        $returnData = mysqli_fetch_array($SingleTable, MYSQLI_ASSOC);

        return $returnData;
    }
    public function updateTable($postData) {
        
        $nameOfPackage = ($_POST['add_nameOfPackage']);
        $packageTitle = ($_POST['add_package_title']);
        $price = ($_POST['add_price']);
        $detailsSerialize = array(
            $_POST['details_speed'],
            $_POST['details_direct_speed'],
            $_POST['details_youtube'],
            $_POST['details_ftp'],
            );
        $details = serialize($detailsSerialize); 
        $id = $postData['id'];
        
        if($nameOfPackage == ""){
            return "Please Input package Name";
        }if($packageTitle == ""){
            return "Please Input package Title";
        }elseif ($price == "") {
            return "Please Input package Price";
        }
        
        $nameOfPackage = mysqli_real_escape_string($this->con, $nameOfPackage);
        $price = mysqli_real_escape_string($this->con, $price);
        $details = mysqli_real_escape_string($this->con, $details);
        
        $singleSlider = $this->showSingleTable($id);

        $product_insert_query = "UPDATE `pricing_table` SET `package_name` = '$nameOfPackage', `package_title` = '$packageTitle', `package_price` = '$price', `package_description` = '$details' WHERE `pricing_table`.`id` = $id";
       
        $update_query = mysqli_query($this->con, $product_insert_query) or die(mysql_error());    
        if ($update_query) {
        ?>
        <script>
            window.location = "priceTable.php";
        </script>
        <?php
    } else {
        $error = "Error! Could not Upload your Information";
        
    }
        return $update_query;
    }

}
