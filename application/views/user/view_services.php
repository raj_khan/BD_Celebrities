<?php
include './component/header.php';
include './component/sidebar.php';
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

$allSliderImage = $model->view_all_by_cond('image', 'category_id = 10  ORDER BY `image`.`id` DESC');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">

            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;">Slider Image View</h2>
            </div>

            <!---=======================Data Table=====================------>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Category</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 1;
                            foreach ($allSliderImage as $image) {
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <?php
                                        $viewCategory = $model->details_by_cond('categories
', 'id = ' . $image['category_id'] . '');
                                        echo $viewCategory['category_name'];
                                        ?>
                                    </td>
                                    <td><?php echo $image['title']; ?></td>
                                    <td><?php echo $image['description']; ?></td>
                                    <td>
                                        <img style="width:50px; height:auto;"
                                             src="../assets/img/services/<?php echo isset($image['img']) ? $image['img'] : null ?>">
                                    </td>
                                    <td>
                                        <a href="edit_services.php?services_id=<?php echo $image['id']; ?>">
                                            <button type="button" class="btn btn-success">Edit</button>
                                        </a>
                                        <a onclick="return confirm('Are you sure you want to delete?')"
                                           href="deleteProduct.php?services_id=<?php echo $image['id']; ?>">
                                            <button type="button" class="btn btn-danger">Delete</button>
                                        </a>
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


        </div>
        <!-- /.row -->


    </div>

</div>
<!-- /.content-wrapper -->

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
<?php
include './component/footer.php';
?>
