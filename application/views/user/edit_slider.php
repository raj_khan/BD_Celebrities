<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include './component/header.php';
include './component/sidebar.php';

include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

if (isset($_GET['slider_id'])) {
    $id = $_GET['slider_id'];
}


$viewAllSlider = $model->details_by_cond('image', 'id = '.$id.'');




if (isset($_POST['submit'])) {
    $error = $slider->updateSliderInfo($_POST, $_FILES);

}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Update Slider';?></h2>
            </div>
            <form class="form-horizontal" method="POST"  enctype="multipart/form-data">
                <div class="col-md-8 col-md-offset-2">

                    <div class="form-group">
                        <label>Slider Title : </label>
                        <input type="text" name="slider_title" class="form-control" value="<?php echo $viewAllSlider['title']; ?>">
                    </div>

                    <label>Slider Description: </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">
                            <form>
                                <textarea name="description" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $viewAllSlider['description']; ?></textarea>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 form-group">

                        <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                    </div>
                    <div class="col-md-6" style="padding-bottom: 10px;">
                        <img src="../assets/img/slider_image/<?php echo $viewAllSlider['img'] ?>" style="height: 280px; width: 347px;">
                    </div>

                    <input type="hidden" name="id" value="<?php echo $viewAllSlider['id'] ?>">
                    <input class="btn btn-success" type="submit" name="submit" value="Submit" style="float: right">
                </div>
            </form>

        </div>
        <!-- /.row -->


    </div>

</div>
<!-- /.content-wrapper -->
<script>
    $("#file-4").fileinput({
        allowedFileExtensions: ['jpg', 'JPEG', 'png', 'gif'],
        showUpload: false,
        /*initialPreview: [
         "http://lorempixel.com/1920/1080/transport/1",
         ],*/
        initialPreviewConfig: [
            {caption: "transport-1.jpg", size: 329892, width: "220px", url: "{$url}", key: 1},
        ]


    });
</script>
<?php
include './component/footer.php';
?>

