<?php
session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/");
}

include './component/header.php';
include './component/sidebar.php';



if (isset($_POST['submit'])) {

    $currentPass = md5($_POST['current_pass']);
    $newPass = md5($_POST['newPass']);
    $newConfirmPass = md5($_POST['newConfirmPass']);

    $userId = $_SESSION["s_id"];
    $query = "SELECT * FROM `admin` WHERE `id` = '$userId'";
    
    $checkUser = mysqli_query($con, $query);
    $userRow = mysqli_fetch_array($checkUser, MYSQLI_ASSOC);

    if (strcmp($userRow['pass'], $currentPass) == 0) {
        if (strcmp($newConfirmPass, $newPass) == 0) {
            $updatePassQuery = "UPDATE `admin` SET `pass` = '$newPass' WHERE `admin`.`id` = $userId;";
            $updatedPassword = mysqli_query($con, $updatePassQuery);
           
            if ($updatedPassword) {
                $notification = "Your password is succesfully changned";
            } else {
                $notification = "Sorry your password is not changed";
            }
        } else {
            $notification = "Sorry Password and Confirm Password doesn't match";
        }
    } else {
        $notification = "Sorry your old password doesn't match";
    }
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="container">
            <div class="row">
                <br>
                <?php if (isset($notification) && !empty($notification)) { ?>
                    <div class="col-md-6 col-md-offset-3">
                        <div class="alert alert-warning alert-bordered">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                            <span class="text-semibold"> <?php echo $notification ?> </span>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;">Password Change</h2>
            </div>
            <form class="form-horizontal" method="POST"  enctype="multipart/form-data">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-group">
                        <label>Old Password : </label>
                        <input type="password" name="current_pass" class="form-control" placeholder="Old Password.....">
                    </div>
                    <div class="form-group">
                        <label>New Password : </label>
                        <input type="password" name="newPass" class="form-control" placeholder="Enter New Password.....">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password : </label>
                        <input type="password" name="newConfirmPass" class="form-control" placeholder="Enter Confirm Password.....">
                    </div>
                    <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                </div>

            </form>
        </div>
        <!-- /.row -->
    </div>
</div>

<?php
include './component/footer.php';
?>