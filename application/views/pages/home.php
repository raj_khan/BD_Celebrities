<div class="main_content container">
    <div class="featured_slider"> 
        <div class="featured_title">
            <h4>ক্রিকেটার </h4>
            <a href="<?php echo base_url(); ?>popular-bangladeshi-cricketers" class="view_button">সকল ক্রিকেটার  </a>
        </div>
        <div class="featured_posts_slider">
            <div id="featured_post">
                <?php $cricketers = $this->Bio_model->view_cricketers_for_home(); ?>
                <?php
                foreach ($cricketers as $cricketer):
                    ?>
                    <div class="item">
                        <div class="img_post">
                            <a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>">
                                <img src="<?php echo base_url(); ?>upload/cricketers/<?php echo $cricketer->img; ?>" alt="<?php echo $cricketer->name; ?>">
                            </a>
                        </div>
                        <div class="featured_title_post">
                            <div class="caption_inner cricketer_title">
                                <a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><h4 class="title_post"><?php echo $cricketer->title; ?></h4></a>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="block_posts block_5">
        <div class="featured_title">
            <h4>অভিনয় শিল্পী</h4>
            <a href="<?php echo base_url(); ?>popular-bangladeshi-actress" class="view_button">সকল অভিনয় শিল্পী </a>
        </div>
        <div class="block_inner">
            <?php $actress = $this->Bio_model->view_actress_bio_for_home(); ?>
            <?php
            foreach ($actress as $actor):
                ?>
                <article class="a-post-box">
                    <a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>">
                    <figure class="latest-img">
                        
                            <img src="<?php echo base_url(); ?>upload/actress/<?php echo $actor->img; ?>" alt="<?php echo $actor->name; ?>" class="latest-cover">
                        
                        
                        </figure>
                        </a>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"><?php echo $actor->title; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>" style="color: #fff;"><?php $date = $actor->created_at; echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="block_posts block_5">
        <div class="featured_title">
            <h4>শিল্পী </h4>
            <a href="<?php echo base_url(); ?>popular-bangladeshi-singers" class="view_button">সকল  শিল্পী  </a>
        </div>
        <div class="block_inner">
            <?php $singers = $this->Bio_model->view_singers_for_home(); ?>
            <?php
            foreach ($singers as $singer):
                ?>
                <article class="a-post-box">
                    <figure class="latest-img"><img
                                src="<?php echo base_url(); ?>upload/singers/<?php echo $singer->img; ?>"
                                alt="<?php echo $singer->name; ?>" class="latest-cover"></figure>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"><?php echo $singer->title; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>" style="color: #fff;"><?php $date = $singer->created_at;
                                    echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
    </div>
</div>