<div class="main_content container">
    <div class="block_posts block_5">
        <div class="block_inner">
            <?php $singers = $this->Bio_model->manage_singers_bio_info(); ?>
            <?php
            foreach ($singers as $singer):
                ?>
                <article class="a-post-box">
                    <figure class="latest-img"><img
                                src="<?php echo base_url(); ?>upload/singers/<?php echo $singer->img; ?>"
                                alt="<?php echo $singer->name; ?>" class="latest-cover"></figure>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"><?php echo $singer->title; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"
                                                      style="color: #fff;"><?php $date = $singer->created_at;
                                    echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
    </div>
</div>