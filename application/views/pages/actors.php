<div class="main_content container">
    <div class="block_posts block_5">
        <div class="block_inner">
            <?php $actress = $this->Bio_model->manage_actress_bio_info(); ?>
            <?php
            foreach ($actress as $actor):
                ?>
                <article class="a-post-box">
                    <figure class="latest-img"><img
                                src="<?php echo base_url(); ?>upload/actress/<?php echo $actor->img; ?>"
                                alt="<?php echo $actor->name; ?>" class="latest-cover"></figure>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"><?php echo $actor->title; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"
                                                      style="color: #fff;"><?php $date = $actor->created_at;
                                    echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
    </div>
</div>