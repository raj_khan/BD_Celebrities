<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<!--> <!--<![endif]-->
<html lang="en">
<head>
    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <!--Website SEO-->
    <title><?php echo isset($title) ? $title : 'BD Celebrities'; ?></title>
    <meta name="keywords"
          content="<?php echo isset($meta_tag) ? $meta_tag : 'BD Celebrities Biography, Bangladeshi Celebrities Bangla Biography, Bangladeshi Celebrities Photo, BD Actress Biography, BD Singers Biography, BD Cricketers Biography, BD Youtubers Biography, BD Popular Business Man Biography, BD Top Politician Biography, BD Educational Leader Biography, BD Top Doctors Biography'; ?>">
    <meta name="description"
          content="<?php echo isset($meta_description) ? $meta_description : 'BD Celebrities Biography, Bangladeshi Celebrities Bangla Biography, Bangladeshi Celebrities Photo, BD Actress Biography, BD Singers Biography, BD Cricketers Biography, BD Youtubers Biography, BD Popular Business Man Biography, BD Top Politician Biography, BD Educational Leader Biography, BD Top Doctors Biography'; ?>">
    <meta property="og:url"                content="<?php echo isset($currentUrl) ? $currentUrl : '' ?>" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="<?php echo isset($title) ? $title : '' ?>" />
    <meta property="og:description"        content="<?php echo isset($meta_description) ? $meta_description : '' ?>" />
    <meta property="og:image"              content="<?php echo base_url()?>upload/<?php echo isset($imageFolder) ? $imageFolder : '' ?>/<?php echo isset($img) ? $img : '' ?>" />
 <meta property=og:image:width content=600 />
<meta property=og:image:height content=315 />
<meta name=twitter:card content=summary_large_image />
<meta name=twitter:site content="@BDCelebrities1"/>
<meta name=twitter:creator content="@BDCelebrities1"/>
<meta name=twitter:url content="<?php echo isset($currentUrl) ? $currentUrl : '' ?>"/>
<meta name=twitter:title content="<?php echo isset($title) ? $title : '' ?>"/>
<meta name=twitter:description content="<?php echo isset($meta_description) ? $meta_description : '' ?>"/>
<meta name=twitter:image content="<?php echo base_url()?>upload/<?php echo isset($imageFolder) ? $imageFolder : '' ?>/<?php echo isset($img) ? $img : '' ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="fb:app_id" content="328027154622810" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>template/frontend/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>template/frontend/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>template/frontend/assets/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>template/frontend/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>template/frontend/assets/css/shopping-page.css">
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>template/frontend/assets/img/favicon.png">
    <script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.v2.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/bootstrap.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126029993-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-126029993-1');
    </script>
</head>
<!--<body oncontextmenu="return false" class="prevent_copy">-->
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="all_content  container-fluid">
    <div class="row">
        <div class="header"><!-- Start header -->
            <div class="top_bar"><!-- Start top_bar -->
                <div class="min_top_bar"><!-- Start min_top_bar -->
                    <div class="container">
                        <div class="top_nav"><!-- Start top_nav -->
                            <ul>
                                <li><a href="<?php echo base_url(); ?>about">About</a></li>
                                <li><a href="<?php echo base_url(); ?>copyright">Copyright</a></li>
                                <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
                            </ul>
                        </div><!-- End top_nav -->

                        <div id="top_search_ico"><!-- Start top_search_ico -->
                            <div class="top_search"><!-- Start top_search -->
                                <form method="get"><input type="text"
                                                          placeholder="Type Celebrity Name and hit enter..."></form>
                                <i class="fa fa-search search-desktop"></i>
                            </div><!-- End top_search -->

                            <div id="top_search_toggle"><!-- Start top_search_toggle -->
                                <div id="search_toggle_top">
                                    <form method="get"><input type="text"
                                                              placeholder="Type Celebrity Name and hit enter..."></form>
                                </div>
                                <i class="fa fa-search search-desktop"></i>
                            </div><!-- End top_search_toggle -->
                        </div><!-- End top_search_ico -->

                        <div class="social_icon"><!-- Start social_icon -->
                            <span><a href="https://www.facebook.com/bdcelebritiesbio/" target="_blank"><i
                                            class="fa fa-facebook"></i></a></span>
                            <span><a href="https://twitter.com/BDCelebrities1/" target="_blank"><i
                                            class="fa fa-twitter"></i></a></span>
                            <span><a href="https://plus.google.com/u/3/108974769044226584058?tab=wX" target="_blank"><i
                                            class="fa fa-google-plus"></i></a></span>
                            <span><a href="https://www.youtube.com/channel/UCaFFb9d88q0K7LqsXh7hqnA" target="_blank"><i
                                            class="fa fa-youtube"></i></a></span>
                            <span><a href="https://www.linkedin.com/company/14451949" target="_blank"><i
                                            class="fa fa-linkedin"></i></a></span>
                            <span><a href="https://www.pinterest.com/bdcelebrities/" target="_blank"><i
                                            class="fa fa-pinterest"></i></a></span>
                        </div><!-- End social_icon -->
                    </div>
                </div><!-- End min_top_bar -->
            </div><!-- End top_bar -->
            <div class="main_header"><!-- Start main_header -->
                <div class="container">
                    <div class="logo logo_blog_layout"><!-- Start logo -->
                        <!-- <h3>logo</h3> -->
                        <a href="<?php echo base_url(); ?>"><img
                                    src="<?php echo base_url(); ?>template/frontend/assets/img/demo/logo.png"
                                    alt="Logo"></a>
                    </div><!-- End logo -->
                    <div class="nav_bar"><!-- Start nav_bar -->
                        <nav id="primary_nav_wrap"><!-- Start primary_nav_wrap -->
                            <ul>
                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>">হোম</a></li>
                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>popular-bangladeshi-cricketers">ক্রিকেটার</a>
                                </li>

                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>popular-bangladeshi-singers">শিল্পী</a></li>

                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>popular-bangladeshi-actress">অভিনয় শিল্পী</a></li>

                                <li>
                                    <a class=""
                                       href="#">বায়োগ্রাফি</a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-actress-bio">অভিনয় শিল্পী</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>top-bangladeshi-cricketers-bio">ক্রিকেটার</a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-singers-bio">শিল্পী</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav><!-- End primary_nav_wrap -->
                    </div><!-- End nav_bar -->
                    <div class="hz_responsive"><!--button for responsive menu-->
                        <div id="dl-menu" class="dl-menuwrapper">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>">হোম</a></li>
                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>popular-bangladeshi-cricketers">ক্রিকেটার</a>
                                </li>

                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>popular-bangladeshi-singers">শিল্পী</a></li>

                                <li>
                                    <a class=""
                                       href="<?php echo base_url(); ?>popular-bangladeshi-actress">অভিনয় শিল্পী</a></li>

                                <li>
                                    <a class=""
                                       href="#">বায়োগ্রাফি</a>
                                    <ul class="dl-submenu">
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-actress-bio">অভিনয় শিল্পী</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>top-bangladeshi-cricketers-bio">ক্রিকেটার</a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-singers-bio">শিল্পী</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div><!-- /dl-menuwrapper -->
                    </div><!--End button for responsive menu-->

                </div>
            </div><!-- End main_header -->
        </div><!-- End header -->
        <?php echo $main_content; ?>
        <div id="footer" class="footer container-fulid"><!-- Start footer -->
            <div class="copyright"> <!-- Start copyright -->
                <div style="background-image: url(<?php echo base_url(); ?>template/frontend/assets/img/arrow.png);"
                     class="hmztop">Scroll To Top
                </div><!-- Back top -->
                <ul style="list-style: none; display: block;">
                    <li style="display: inline-block;"><a href="<?php echo base_url(); ?>about">About</a></li>
                    <li><a href="<?php echo base_url(); ?>copyright">Copyright</a></li>
                    <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
                </ul>
                <div class="social_icon"><!--Start social_icon -->
                    <span><a href="https://www.facebook.com/bdcelebritiesbio/" target="_blank"><i
                                    class="fa fa-facebook"></i></a></span>
                    <span><a href="https://twitter.com/BDCelebrities1/" target="_blank"><i
                                    class="fa fa-twitter"></i></a></span>
                    <span><a href="https://plus.google.com/u/3/108974769044226584058?tab=wX" target="_blank"><i
                                    class="fa fa-google-plus"></i></a></span>
                    <span><a href="https://www.youtube.com/channel/UCaFFb9d88q0K7LqsXh7hqnA" target="_blank"><i
                                    class="fa fa-youtube"></i></a></span>
                    <span><a href="https://www.linkedin.com/company/14451949" target="_blank"><i
                                    class="fa fa-linkedin"></i></a></span>
                    <span><a href="https://www.pinterest.com/bdcelebrities/" target="_blank"><i
                                    class="fa fa-pinterest"></i></a></span>
                </div><!--End social_icon -->
                <p>Copyrights © <?php echo date("Y"); ?> All Rights Reserved by <a href="<?php echo base_url(); ?> ">BD
                        Celebrities</a></p>
            </div><!-- End copyright -->
        </div><!-- End footer -->
    </div><!-- End row -->
</div><!-- End all_content -->
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/isotope.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.jribbble-1.0.1.ugly.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/hamzh.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/shopping_page.js"></script>
<script type="text/javascript">
    jQuery(function ($) {
        $("ul a")
            .click(function(e) {
                var link = $(this);
                var item = link.parent("li");

                if (item.hasClass("current-menu-item")) {
                    item.removeClass("current-menu-item").children("a").removeClass("current-menu-item");
                } else {
                    item.addClass("current-menu-item").children("a").addClass("current-menu-item");
                }

                if (item.children("ul").length > 0) {
                    var href = link.attr("href");
                    link.attr("href", "#");
                    setTimeout(function () {
                        link.attr("href", href);
                    }, 300);
                    e.preventDefault();
                }
            })
            .each(function() {
                var link = $(this);
                if (link.get(0).href === location.href) {
                    link.addClass("current-menu-item").parents("li").addClass("current-menu-item");
                    return false;
                }
            });
    });
</script>
</body>
</html>
