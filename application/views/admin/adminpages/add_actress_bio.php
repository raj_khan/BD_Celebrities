<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">

        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center"style="font-family: monospace; font-weight: bold;">Insert Actress Bio</h2>

            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo base_url('Bio/save_actress_bio'); ?>">
                <div class="col-md-8 col-md-offset-2">


                    <div class="form-group">
                        <label>  Select Actress</label>
                        <select name="actress_category" class="form-control select2" style="width: 100%;">
                            <?php $actress_category_info = $this->Bio_model->show_publish_actress_category(); ?>
                            <?php foreach ($actress_category_info as  $category): ?>
                                <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                            <?php endforeach;  ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Title  </label>
                        <input type="text" name="title" class="form-control" placeholder="Enter  Title.....">
                    </div>


                    <div class="form-group">
                        <label>Actress  Name </label>
                        <input type="text" name="name" class="form-control" placeholder="Enter  Name.....">
                    </div>


                    <label>Intro/ শুরু কথাঃ  </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="intro" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>




                    <label>Early Years/ প্রাথমিক জীবনঃ  </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="early_life" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>


                    <label>Personal Life/ ব্যক্তিগত জীবনঃ </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="personal_life" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>


                    <label>Career </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="career" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>

                    <label>Filmography </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="filmography" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>



                    <label>Awards/সম্মাননাঃ </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="awards" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>




                    <label>Image/ছবিঃ </label>
                    <div class="form-group">
                        <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                    </div>


                    <div class="form-group">
                        <label>Publication Status/প্রকাশনা অবস্থা  </label>
                        <select name="status" class="form-control select2" style="width: 100%;">
                            <option value="1">PUBLISH</option>
                            <option value="0">UN-PUBLISH</option>
                        </select>
                    </div>


                    <label>Meta Keywords</label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="meta_tag" class="" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>



                    <label>Meta Description </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="meta_description" class="" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                        </div>
                    </div>

                    <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                </div>

            </form>

        </div>
        <!-- /.row -->


    </div>

</div>