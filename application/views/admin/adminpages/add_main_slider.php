<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">

        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center"style="font-family: monospace; font-weight: bold;">Insert Main Slider</h2>

            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="<?php echo base_url('Common_images/save_main_slider'); ?>">
                <div class="col-md-8 col-md-offset-2">



                    <div class="form-group">
                        <label>Title </label>
                        <input type="text" name="title" class="form-control" placeholder="Enter  Title.....">
                    </div>


                    <label>Image/ছবিঃ </label>
                    <div class="form-group">
                        <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                    </div>


                    <div class="form-group">
                        <label>Publication Status/প্রকাশনা অবস্থা  </label>
                        <select name="status" class="form-control select2" style="width: 100%;">
                            <option value="1">PUBLISH</option>
                            <option value="0">UN-PUBLISH</option>
                        </select>
                    </div>

                    <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                </div>

            </form>

        </div>
        <!-- /.row -->


    </div>

</div>