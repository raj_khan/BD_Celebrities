<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">

        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;"> Update Main Slider</h2>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                  action="<?php echo base_url('Common_images/update_main_slider'); ?>">
                <div class="col-md-8 col-md-offset-2">

                    

                    <div class="form-group">
                        <label> Title : </label>
                        <input type="hidden" name="id" value="<?php echo $main_slider_info->id; ?>" class="form-control">
                        <input type="text" name="title" value="<?php echo $main_slider_info->title; ?>" class="form-control">
                    </div>


                    <div class="form-group">
                        <label>Publication Status : </label>
                        <select name="status" class="form-control select2" style="width: 100%;">
                            <option value="1">PUBLISH</option>
                            <option value="0">UN-PUBLISH</option>
                        </select>
                    </div>

                    <div class="col-md-6 form-group">

                        <input type="hidden"   name="old"  value="<?php echo $main_slider_info->img  ?>">

                        <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                    </div>
                    <div class="col-md-6" style="padding-bottom: 10px;">
                        <img src="<?php echo base_url();?>upload/slider/<?php echo $main_slider_info->img ?>" style="height: 280px; width: 347px;">
                    </div>




                    <input class="btn btn-success" type="submit" name="submit" value="Update" style="float: right">
                </div>

            </form>

        </div>
        <!-- /.row -->


    </div>

</div>