<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">

            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;"> Main Slider View</h2>
            </div>

            <!---=======================Data Table=====================------>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Title</th>
                                <th>Img</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>



                            <?php $categories = $this->Common_images_model->manage_main_slider_info(); ?>

                            <?php $i = 1;
                            foreach ($categories as $category):
                                ?>

                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $category->title; ?></td>
                                    <td>
                                        <img style="width:50px; height:auto;"
                                             src="upload/slider/<?php echo $category->img; ?>">
                                    </td>
                                    <td>
                                        <?php if ($category->status == 1): ?>
                                            <a href="<?php echo base_url(); ?>un-publish-main-slider/<?php echo $category->id; ?>">
                                                <button type="button" class="btn btn-danger">Inactive</button>
                                            </a>
                                        <?php else: ?>
                                            <a href="<?php echo base_url(); ?>publish-main-slider/<?php echo $category->id; ?>">
                                                <button type="button" class="btn btn-success">Active</button>
                                            </a>
                                        <?php endif; ?>
                                    </td>

                                    <td>
                                        <a href="<?php echo base_url(); ?>update-main-slider/<?php echo $category->id; ?>">
                                            <button type="button" class="btn btn-success">Edit</button>
                                        </a>
                                        <a href="<?php echo base_url(); ?>delete-main-slider/<?php echo $category->id; ?>">
                                            <button type="button" class="btn btn-danger"
                                                    onclick="return confirm('Are you sure you want to delete?')">Delete
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++; endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


        </div>
        <!-- /.row -->


    </div>

</div>

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>