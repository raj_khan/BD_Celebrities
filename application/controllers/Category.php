<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Category_model');
        $admin_id = $this->session->userdata('id');
        if($admin_id == NULL){
            redirect('admin/admin', 'refresh');
        }
    }

    public function add_category(){
        $data = array();
        $data['dashboard_content'] = $this->load->view('admin/adminpages/add_category', '', TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    // Save Category
    public function save_category(){
        $this->Category_model->save_category_info();

        $mData = array();
        $mData['message'] = "Successfully Save...";
        $this->session->set_userdata($mData);

        redirect('Category/add_category');
    }


    //Category Manage
    public function manage_category(){
        $data = array();
        $data['all_category_info'] = $this->Category_model->manage_category_info();

        $data['dashboard_content'] = $this->load->view('admin/adminpages/manage_category', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    //Publish Category
    public function publish_category($id){
        $this->Category_model->publish_category_info($id);
        redirect('view-all-category');
    }


    //Un-publish Category
    public function un_publish_category($id){
        $this->Category_model->un_publish_category_info($id);
        redirect('view-all-category');
    }

    // Category Delete
    public function delete_category($id){
        $this->Category_model->delete_category_info($id);
        redirect('view-all-category');
    }

    // Show Data For  Category Update
    public function show_for_update_category($id){
        $data = array();
        $data['category_info'] = $this->Category_model->show_for_update_category_info($id);
        $data['dashboard_content'] = $this->load->view('admin/adminpages/update_category', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    //  Category Update
    public function update_category(){
        $this->Category_model->update_category_info();
        redirect('view-all-category');
    }




    //============================Celebrity Category=====================

    public function add_celebrity_category(){
        $data = array();
        $data['dashboard_content'] = $this->load->view('admin/adminpages/add_celebrity_category', '', TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    // Save Category
    public function save_celebrity_category(){
        $this->Category_model->save_celebrity_category_info();

        $mData = array();
        $mData['message'] = "Successfully Save...";
        $this->session->set_userdata($mData);

        redirect('Category/add_celebrity_category');
    }


    //Category Manage
    public function manage_celebrity_category(){
        $data = array();
        $data['celebrity_category_info'] = $this->Category_model->manage_celebrity_category_info();

        $data['dashboard_content'] = $this->load->view('admin/adminpages/manage_celebrity_category', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }


    //Publish
    public function publish_celebrity_category($id){
        $this->Category_model->publish_celebrity_category_info($id);
        redirect('view-celebrity-category');
    }


    //Un-publish
    public function un_publish_celebrity_category($id){
        $this->Category_model->un_publish_celebrity_category_info($id);
        redirect('view-celebrity-category');
    }


    //  Delete
    public function delete_celebrity_category($id){
        $this->Category_model->delete_celebrity_category_info($id);
        redirect('view-celebrity-category');
    }


    // Show Data For  Update
    public function show_for_update_celebrity_category($celebrity_id){
        $data = array();
        $data['category_info'] = $this->Category_model->show_for_update_celebrity_category_info($celebrity_id);
        $data['dashboard_content'] = $this->load->view('admin/adminpages/update_celebrity_category', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }



    //   Update
    public function update_celebrity_category(){
        $this->Category_model->update_celebrity_category_info();
        redirect('view-celebrity-category');
    }


}