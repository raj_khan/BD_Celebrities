<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_images extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Common_images_model');
        $admin_id = $this->session->userdata('id');
        if ($admin_id == NULL) {
            redirect('admin', 'refresh');
        }
    }

    // Main Slider Page View
    public function add_main_slider(){
            $data = array();
            $data['dashboard_content'] = $this->load->view('admin/adminpages/add_main_slider', '', TRUE);
            $this->load->view('admin/dashboard_master', $data);

    }
//save_main_slider
    public function save_main_slider(){
        $this->Common_images_model->save_main_slider_info();

        $mData = array();
        $mData['message'] = "Successfully Save...";
        $this->session->set_userdata($mData);

        redirect('Common_images/add_main_slider');
    }


//manage_main_slider
    public function manage_main_slider()
    {
        $data = array();
        $data['main_slider_info'] = $this->Common_images_model->manage_main_slider_info();

        $data['dashboard_content'] = $this->load->view('admin/adminpages/manage_main_slider', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }




    //un_publish_main_slider
    public function un_publish_main_slider($id){
        $this->Common_images_model->un_publish_main_slider_info($id);
        redirect('view-main-slider');
    }

    //publish_main_slider
    public function publish_main_slider($id){
        $this->Common_images_model->publish_main_slider_info($id);
        redirect('view-main-slider');
    }

    // Main Slider  Delete
    public function delete_main_slider($id){
        $this->Common_images_model->delete_main_slider_info($id);
        redirect('view-main-slider');
    }


//show for update main slider
    public function show_for_update_main_slider($id)
    {
        $data = array();
        $data['main_slider_info'] = $this->Common_images_model->show_for_update_main_slider_info($id);
        $data['dashboard_content'] = $this->load->view('admin/adminpages/update_main_slider', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }


    //  Main Slider Update
    public function update_main_slider()
    {
        $this->Common_images_model->update_main_slider_info();
        redirect('view-main-slider');
    }





}