<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        $admin_id = $this->session->userdata('id');
        if($admin_id != NULL){
            redirect('Super_admin', 'refresh');
        }
    }

    public function index()
    {
        $this->load->view('admin_login');
    }

    public function admin_login_check()
    {
        $email = $this->input->post('email');
        $pass = $this->input->post('pass');



        $result = $this->Admin_model->admin_login_check_info($email, $pass);


        $session_data = array();

        if($result){

            foreach ($result as $key => $value){
                $session_data['id'] = $value['id'];
                $session_data['name'] = $value['name'];
            }
            $this->session->set_userdata($session_data);

            redirect('Super_admin');
        }else{

            $session_data['login_message'] = "Invalid Login";
            $this->session->set_userdata($session_data);

            redirect('admin');
        }
    }
}