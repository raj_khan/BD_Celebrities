-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2018 at 02:11 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd_celebrities`
--

-- --------------------------------------------------------

--
-- Table structure for table `actors_bio`
--

CREATE TABLE `actors_bio` (
  `id` int(11) NOT NULL,
  `celebrity_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `intro` text,
  `early_life` text,
  `personal_life` text,
  `career` text,
  `filmography` text,
  `awards` text,
  `img` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL COMMENT 'status = 1 Active, Status = 0 Inactive	',
  `meta_tag` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actors_bio`
--

INSERT INTO `actors_bio` (`id`, `celebrity_id`, `name`, `intro`, `early_life`, `personal_life`, `career`, `filmography`, `awards`, `img`, `status`, `meta_tag`, `meta_description`, `created_at`, `updated_at`) VALUES
(7, 13, 'Pori Moni', '', '', '', '', '', '', '24174539_1715345295184512_8441144134389861957_n.jpg', 1, '<p>dsaf</p>', '<p>afsda</p>', '2018-09-18 09:51:24', '2018-09-18 09:51:24');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `celebrity_id` int(11) NOT NULL,
  `title` varchar(200) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `celebrity_categories`
--

CREATE TABLE `celebrity_categories` (
  `id` int(11) NOT NULL,
  `main_category_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT 'status = 1 Active, Status =  0 Inactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `celebrity_categories`
--

INSERT INTO `celebrity_categories` (`id`, `main_category_id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(13, 3, 'Pori Moni', '', 1, '2018-09-18 09:47:03', '2018-09-18 09:47:03');

-- --------------------------------------------------------

--
-- Table structure for table `cricketers_bio`
--

CREATE TABLE `cricketers_bio` (
  `id` int(11) NOT NULL,
  `celebrity_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `intro` text,
  `early_years` text,
  `personal_life` text,
  `domestic_cricket` text,
  `international_career` text,
  `achivment` text,
  `awards` text,
  `img` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL COMMENT 'status = 1 Active, Status =  0 Inactive',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cricketers', '<p>This is Cricketers Category. </p>', 1, '2018-07-10 10:32:23', '2018-07-10 10:32:23'),
(2, 'Singers ', '<p>This is Singers Category</p>', 1, '2018-07-10 10:32:37', '2018-07-10 10:32:37'),
(3, 'Actors', '<p>This is Actors Category</p>', 1, '2018-07-10 10:32:49', '2018-07-10 10:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `singers_bio`
--

CREATE TABLE `singers_bio` (
  `id` int(11) NOT NULL,
  `celebrity_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `intro` text,
  `early_life` text,
  `personal_life` text,
  `career` text,
  `discography` text,
  `awards` text,
  `img` varchar(200) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT 'status = 1 Active, Status = 0 Inactive	',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `main_category_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `img` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL COMMENT 'status = 1 Active, Status = 0 Inactive	',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `access_level` tinyint(1) NOT NULL COMMENT 'access_level = 1 Super Admin, access_level=2  admin',
  `activation_status` tinyint(1) NOT NULL COMMENT 'status = 1 Active, Status =  0 Inactive',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `pass`, `access_level`, `activation_status`, `created_at`, `updated_at`) VALUES
(1, 'Raj Khan', 'raj@gmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, '2018-06-09 05:11:22', '2018-06-09 05:11:22'),
(2, 'Tanmoy Kumar', 'tonmoy@gmail.com', '202cb962ac59075b964b07152d234b70', 2, 1, '2018-06-09 05:11:22', '2018-06-09 05:11:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actors_bio`
--
ALTER TABLE `actors_bio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `celebrity_categories`
--
ALTER TABLE `celebrity_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cricketers_bio`
--
ALTER TABLE `cricketers_bio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `singers_bio`
--
ALTER TABLE `singers_bio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actors_bio`
--
ALTER TABLE `actors_bio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `celebrity_categories`
--
ALTER TABLE `celebrity_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `cricketers_bio`
--
ALTER TABLE `cricketers_bio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `singers_bio`
--
ALTER TABLE `singers_bio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
