<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Super_admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin_model');
        $admin_id = $this->session->userdata('id');
        if($admin_id == NULL){
            redirect('admin', 'refresh');
        }
    }

    public function index(){
        $data = array();
        $data['dashboard_content'] = $this->load->view('admin/dashboard_content', '', TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }


    public function logout(){
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $session_data['login_message'] = "Successfully logged out";
        $this->session->set_userdata($session_data);
        redirect(admin);
    }
}