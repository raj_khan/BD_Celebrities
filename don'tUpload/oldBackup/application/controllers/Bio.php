<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bio extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Bio_model');
        $this->load->library('image_lib');
        $admin_id = $this->session->userdata('id');
        if ($admin_id == NULL) {
            redirect('admin', 'refresh');
        }
    }


    //######################Cricketers Bio##############################

    // cricketers page viewer
    public function add_cricketers_bio()
    {
        $data = array();
        $data['dashboard_content'] = $this->load->view('admin/adminpages/add_cricketers_bio', '', TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }


    // Save Cricketers Bio
    public function save_cricketers_bio()
    {
        $this->Bio_model->save_cricketers_bio_info();

        $mData = array();
        $mData['message'] = "Successfully Save...";
        $this->session->set_userdata($mData);

        redirect('Bio/add_cricketers_bio');
    }

//    Manage Cricketers Bio
    public function manage_cricketers_bio()
    {
        $data = array();
        $data['all_cricketers_info'] = $this->Bio_model->manage_cricketers_bio_info();

        $data['dashboard_content'] = $this->load->view('admin/adminpages/manage_cricketers_bio', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

//show_for_update_cricketers_bio
    public function show_for_update_cricketers_bio($id)
    {
        $data = array();
        $data['cricketers_info'] = $this->Bio_model->show_for_update_cricketers_bio_info($id);
        $data['dashboard_content'] = $this->load->view('admin/adminpages/update_cricketers_bio', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    //  Cricketers Bio Update
    public function update_cricketers_bio()
    {
        $this->Bio_model->update_cricketers_bio_info();
        redirect('view-cricketers-bio');
    }

    // Cricketers Bio  Delete
    public function delete_cricketers_bio($id){
        $this->Bio_model->delete_cricketers_bio_info($id);
        redirect('view-cricketers-bio');
    }
//un_publish_cricketers_bio
    public function un_publish_cricketers_bio($id){
        $this->Bio_model->un_publish_cricketers_bio_info($id);
        redirect('view-cricketers-bio');
    }

//publish_cricketers_bio
    public function publish_cricketers_bio($id){
        $this->Bio_model->publish_cricketers_bio_info($id);
        redirect('view-cricketers-bio');
    }
    //######################Cricketers Bio END##############################


    //######################Actress Bio Start##############################
    // actress bio form page viewer
    public function add_actress_bio()
    {
        $data = array();
        $data['dashboard_content'] = $this->load->view('admin/adminpages/add_actress_bio', '', TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    // Save Actress Bio
    public function save_actress_bio()
    {
        $this->Bio_model->save_actress_bio_info();

        $mData = array();
        $mData['message'] = "Successfully Save...";
        $this->session->set_userdata($mData);

        redirect('Bio/add_actress_bio');
    }


//    Manage actress Bio
    public function manage_actress_bio()
    {
        $data = array();
        $data['actress_info'] = $this->Bio_model->manage_actress_bio_info();

        $data['dashboard_content'] = $this->load->view('admin/adminpages/manage_actress_bio', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }





//show_for_update_actress_bio
    public function show_for_update_actress_bio($id)
    {
        $data = array();
        $data['actress_info'] = $this->Bio_model->show_for_update_actress_bio_info($id);
        $data['dashboard_content'] = $this->load->view('admin/adminpages/update_actress_bio', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }



    //  actress Bio Update
    public function update_actress_bio()
    {
        $this->Bio_model->update_actress_bio_info();
        redirect('view-actress-bio');
    }

    // actress Bio  Delete
    public function delete_actress_bio($id){
        $this->Bio_model->delete_actress_bio_info($id);
        redirect('view-actress-bio');
    }
//un_publish_actress_bio
    public function un_publish_actress_bio($id){
        $this->Bio_model->un_publish_actress_bio_info($id);
        redirect('view-actress-bio');
    }

//publish_actress_bio
    public function publish_actress_bio($id){
        $this->Bio_model->publish_actress_bio_info($id);
        redirect('view-actress-bio');
    }





    //######################Actress Bio END##############################




    //######################singers Bio Start##############################
    // actress bio form page viewer
    public function add_singers_bio()
    {
        $data = array();
        $data['dashboard_content'] = $this->load->view('admin/adminpages/add_singers_bio', '', TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    // Save singers Bio
    public function save_singers_bio()
    {
        $this->Bio_model->save_singers_bio_info();
        $mData = array();
        $mData['message'] = "Successfully Save...";
        $this->session->set_userdata($mData);

        redirect('Bio/add_singers_bio');
    }


//    Manage singers Bio
    public function manage_singers_bio()
    {
        $data = array();
        $data['singers_info'] = $this->Bio_model->manage_singers_bio_info();

        $data['dashboard_content'] = $this->load->view('admin/adminpages/manage_singers_bio', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }





//show_for_update_singers_bio
    public function show_for_update_singers_bio($id)
    {
        $data = array();
        $data['singers_info'] = $this->Bio_model->show_for_update_singers_bio_info($id);
        $data['dashboard_content'] = $this->load->view('admin/adminpages/update_singers_bio', $data, TRUE);
        $this->load->view('admin/dashboard_master', $data);
    }

    //  singers Bio Update
    public function update_singers_bio()
    {
        $this->Bio_model->update_singers_bio_info();
        redirect('view-singers-bio');
    }

    // singers Bio  Delete
    public function delete_singers_bio($id){
        $this->Bio_model->delete_singers_bio_info($id);
        redirect('view-singers-bio');
    }
//un_publish_singers_bio
    public function un_publish_singers_bio($id){
        $this->Bio_model->un_publish_singers_bio_info($id);
        redirect('view-singers-bio');
    }

//publish_singers_bio
    public function publish_singers_bio($id){
        $this->Bio_model->publish_singers_bio_info($id);
        redirect('view-singers-bio');
    }

    //######################singers Bio END##############################


}