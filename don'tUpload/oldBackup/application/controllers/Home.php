<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Bio_model');
    }


//    Home Page
    public function index() {
        $data = array();
        $data['title'] = "HOME-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/home', '', true);
        $this->load->view('frontend_master', $data);

    }



//    About Page
    public function editor() {
        $data = array();
        $data['title'] = "ABOUT-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/contact', '', true);
        $this->load->view('frontend_master', $data);

    }

//    Contact Page
    public function contact() {
        $data = array();
        $data['title'] = "Contact-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/contact', '', true);
        $this->load->view('frontend_master', $data);

    }

//    cricketers Page
    public function cricketers() {
        $data = array();
        $data['title'] = "Popular Cricketers-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/cricketers', '', true);
        $this->load->view('frontend_master', $data);

    }

//    actors Page
    public function actors() {
        $data = array();
        $data['title'] = "Popular Actress-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/actors', '', true);
        $this->load->view('frontend_master', $data);

    }

//    singers Page
    public function singers() {
        $data = array();
        $data['title'] = "Popular Singers-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/singers', '', true);
        $this->load->view('frontend_master', $data);

    }

//    singers Bio Page
    public function singers_bio() {
        $data = array();
        $data['title'] = "Singers Bio-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/singers', '', true);
        $this->load->view('frontend_master', $data);

    }

//    Actors Bio Page
    public function actress_bio() {
        $data = array();
        $data['title'] = "Actress Bio-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/actors', '', true);
        $this->load->view('frontend_master', $data);

    }

//    Cricketers Bio Page
    public function cricketers_bio() {
        $data = array();
        $data['title'] = "Cricketers Bio-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/cricketers', '', true);
        $this->load->view('frontend_master', $data);

    }

//    Cricketers Bio Page
    public function details() {
        $data = array();
        $data['title'] = "Details-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/details', '', true);
        $this->load->view('frontend_master', $data);

    }

//    Copyright  Page
    public function copyright() {
        $data = array();
        $data['title'] = "Copyright-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/copyright', '', true);
        $this->load->view('frontend_master', $data);

    }


//    Privacy Policy Page
    public function privacy_policy() {
        $data = array();
        $data['title'] = "Privacy-Policy-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/privacy-policy', '', true);
        $this->load->view('frontend_master', $data);

    }

//    About Page
    public function about() {
        $data = array();
        $data['title'] = "About-BD Celebrities";
        $data['main_content'] = $this->load->view('pages/about', '', true);
        $this->load->view('frontend_master', $data);

    }


    // ========================Details Page View=====================

    //# Actress Details Page View
    public function show_actress_details_bio($id, $title)
    {
        $data = array();
        $data['currentUrl'] = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $data['actress_details'] = $this->Bio_model->show_actress_details_bio_info($id);
        $data['meta_tag'] = $this->Bio_model->show_actress_details_bio_info($id)->meta_tag;
        $data['meta_description'] = $this->Bio_model->show_actress_details_bio_info($id)->meta_description;
        $data['img'] = $this->Bio_model->show_actress_details_bio_info($id)->img;
        $data['description'] = $this->Bio_model->show_actress_details_bio_info($id)->intro;
        $data['title'] = $this->Bio_model->show_actress_details_bio_info($id)->name;
        $data['main_content'] = $this->load->view('pages/actressDetails', $data, TRUE);
        $this->load->view('frontend_master', $data);
    }


    //# Cricketers Details Page View
    public function show_cricketers_details_bio($id, $title)
    {
        $data = array();
        $data['cricketers_details'] = $this->Bio_model->show_cricketers_details_bio_info($id);
        $data['meta_tag'] = $this->Bio_model->show_cricketers_details_bio_info($id)->meta_tag;
        $data['meta_description'] = $this->Bio_model->show_cricketers_details_bio_info($id)->meta_description;
        $data['title'] =  $this->Bio_model->show_cricketers_details_bio_info($id)->name;
        $data['main_content'] = $this->load->view('pages/cricketersDetails', $data, TRUE);
        $this->load->view('frontend_master', $data);
    }


    //# Singers Details Page View
    public function show_singers_details_bio($id, $title)
    {
        $data = array();
        $data['singers_details'] = $this->Bio_model->show_singers_details_bio_info($id);
        $data['meta_tag'] = $this->Bio_model->show_singers_details_bio_info($id)->meta_tag;
        $data['meta_description'] = $this->Bio_model->show_singers_details_bio_info($id)->meta_description;
        $data['title'] =  $this->Bio_model->show_singers_details_bio_info($id)->name;
        $data['main_content'] = $this->load->view('pages/singersDetails', $data, TRUE);
        $this->load->view('frontend_master', $data);
    }
}