<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common_images_model extends CI_Model
{
    
//==================Main Slider====================
//save main slider info
public function save_main_slider_info(){
    $data = array();

    $data['title'] = $this->input->post('title', TRUE);

    $config['upload_path'] = './upload/slider/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = 100000;
    $config['max_width'] = 10024;
    $config['max_height'] = 76008;

    $this->load->library('upload', $config);

    if (!$this->upload->do_upload('file')) {
        $error = array('error' => $this->upload->display_errors());

        $this->load->view('upload_form', $error);
    } else {
        $uploadData = $this->upload->data();

        //Resize
        $this->main_slider_image_resize('./upload/slider/' .$uploadData['file_name'], $uploadData['file_name']);

        $data['img'] = $uploadData['file_name'];
    }

    $data['status'] = $this->input->post('status', TRUE);

//        =============Inserted To Table=================
    $this->db->insert('sliders', $data);
}

//     manage_main_slider_info
    public function manage_main_slider_info()
    {
        $this->db->select('*');
        $this->db->from('sliders');
        $result_query = $this->db->get();
        $category_info = $result_query->result();

        return $category_info;
    }


//un_publish_main_slider
    public function un_publish_main_slider_info($id)
    {
        $this->db->set('status', 0);
        $this->db->where('id', $id);
        $this->db->update('sliders');
    }


//publish_main_slider
    public function publish_main_slider_info($id)
    {
        $this->db->set('status', 1);
        $this->db->where('id', $id);
        $this->db->update('sliders');
    }

    // delete Main Slider
    public function delete_main_slider_info($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sliders');
    }


    // show_for_update_main_slider_info
    public function show_for_update_main_slider_info($id)
    {
        $this->db->select('*');
        $this->db->from('sliders');
        $this->db->where('id', $id);
        $query_result = $this->db->get();
        $main_slider_info = $query_result->row();
        return $main_slider_info;
    }



    // update_main_slider
    public function update_main_slider_info()
    {
        $data = array();

        $data['id'] = $this->input->post('id', TRUE);
        $data['title'] = $this->input->post('title', TRUE);

        if ($_FILES['file']['name'] != "") {
            $config['upload_path'] = './upload/slider/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
                $this->load->view('upload_form', $error);
            } else {
                $uploadData = $this->upload->data();

                //Resize
                $this->main_slider_image_resize('./upload/slider/' .$uploadData['file_name'], $uploadData['file_name']);

                $data['img'] = $uploadData['file_name'];
            }
        } else {
            $data['img'] = $this->input->post('old');
        }

        $data['status'] = $this->input->post('status', TRUE);

        $this->db->where('id', $data['id']);
        $this->db->update('sliders', $data);
    }

    //#main_slider_image_resize Resize
    public function main_slider_image_resize($path, $file) {
        $config_resize = array();
        $config_resize['image_library'] = 'gd2';
        $config_resize['source_image'] = $path;
        $config_resize['create_thumb'] =  FALSE;
        $config_resize['maintain_ratio'] =  TRUE;
        $config_resize['quality'] =  '90%';
        $config_resize['height']          =  270;
        $config_resize['new_image']          = './upload/slider/' . $file;
        $config_resize['wm_text'] = 'BD Celebrities';
        $config_resize['wm_type'] = 'text';
        $config_resize['wm_font_path'] = './system/fonts/texb.ttf';
        $config_resize['wm_font_size'] = '16';
        $config_resize['wm_font_color'] = 'ffffff';
        $config_resize['wm_vrt_alignment'] = 'top';
        $config_resize['wm_hor_alignment'] = 'center';
        $config_resize['wm_padding'] = '20';
        $this->image_lib->initialize($config_resize);
        $this->load->library('image_lib', $config_resize);
        $this->image_lib->resize();
        $this->image_lib->watermark();
    }


}