<div class="main_content container">

    <!--=============================Cricketers==========================-->
    <div class="featured_slider"> <!--Start featured_slider -->
        <div class="featured_title"><!-- featured_title -->
            <h4>Cricketers</h4>
            <a href="<?php echo base_url(); ?>popular-bangladeshi-cricketers" class="view_button">View All</a>
        </div><!-- // featured_title -->

        <div class="featured_posts_slider"><!-- featured_posts_slider -->
            <div id="featured_post"><!-- featured_post -->

                <?php $cricketers = $this->Bio_model->view_cricketers_for_home(); ?>

                <?php
                foreach ($cricketers as $cricketer):
                    ?>
                    <div class="item"><!-- item -->
                        <div class="img_post">
                            <a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>">
                                <img src="<?php echo base_url(); ?>upload/cricketers/<?php echo $cricketer->img; ?>" alt="<?php echo $cricketer->name; ?>">
                            </a>
                        </div>
                        <div class="featured_title_post">
                            <div class="caption_inner">
                                <a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><h4 class="title_post"><?php echo $cricketer->name; ?></h4></a>
                                <div class="post_date"><em><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><?php $date = $cricketer->created_at;
                                            echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                                <span class="latest-cat"><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>">Read More</a></span>
                            </div>
                        </div>
                    </div><!-- // item -->
                <?php endforeach; ?>


            </div><!-- // featured_post -->
        </div><!-- // featured_posts_slider -->
    </div><!--End featured_slider -->

    <!--===========================ad==================-->
    <!--    <div class="ads_in_block">-->
    <!--        <a href="#"><img src="--><?php //echo base_url(); ?><!--template/frontend/assets/img/ads-728x90.jpg" alt="ads"></a>-->
    <!--    </div>-->

    <!--=============================Actress==========================-->
    <div class="block_posts block_5">
        <!-- featured_title -->
        <div class="featured_title">
            <h4>Actress</h4>
            <a href="<?php echo base_url(); ?>popular-bangladeshi-actress" class="view_button">View All</a>
        </div>
        <!-- // featured_title -->

        <!-- block_inner -->
        <div class="block_inner">

            <?php $actress = $this->Bio_model->view_actress_bio_for_home(); ?>

            <?php
            foreach ($actress as $actor):
                ?>

                <article class="a-post-box">
                    <figure class="latest-img"><img src="<?php echo base_url(); ?>upload/actress/<?php echo $actor->img; ?>" alt="<?php echo $actor->name; ?>" class="latest-cover"></figure>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"><?php echo $actor->name; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>" style="color: #fff;"><?php $date = $actor->created_at; echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
        <!-- // block_inner -->
    </div>

    <!--=============================Singers==========================-->
    <div class="block_posts block_5">
        <!-- featured_title -->
        <div class="featured_title">
            <h4>Singers</h4>
            <a href="<?php echo base_url(); ?>popular-bangladeshi-singers" class="view_button">View All</a>
        </div>
        <!-- // featured_title -->

        <!-- block_inner -->
        <div class="block_inner">

            <?php $singers = $this->Bio_model->view_singers_for_home(); ?>

            <?php
            foreach ($singers as $singer):
                ?>

                <article class="a-post-box">
                    <figure class="latest-img"><img
                                src="<?php echo base_url(); ?>upload/singers/<?php echo $singer->img; ?>"
                                alt="<?php echo $singer->name; ?>" class="latest-cover"></figure>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"><?php echo $singer->name; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>" style="color: #fff;"><?php $date = $singer->created_at;
                                    echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
        <!-- // block_inner -->
    </div>


</div><!-- main_content -->