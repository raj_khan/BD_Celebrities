<div class="main_content container">

    <!--=============================Ad==========================-->

<!--    <div class="ads_in_block">-->
<!--        <a href="#"><img src="--><?php //echo base_url(); ?><!--template/frontend/assets/img/ads-728x90.jpg" alt="ads"></a>-->
<!--    </div>-->


    <!--=============================Cricketers==========================-->
    <div class="block_posts block_5">


        <!-- block_inner -->
        <div class="block_inner">

            <?php $cricketers = $this->Bio_model->manage_cricketers_bio_info(); ?>

            <?php
            foreach ($cricketers as $cricketer):
                ?>

                <article class="a-post-box">
                    <figure class="latest-img">
                             <img src="<?php echo base_url(); ?>upload/cricketers/<?php echo $cricketer->img; ?>"  alt="<?php echo $cricketer->name; ?>" class="latest-cover">
                    </figure>
                    <div class="latest-overlay"></div>
                    <div class="latest-txt">
                        <h5 class="latest-title"><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><?php echo $cricketer->name; ?></a>
                        </h5>
                        <div class="post_date"><em><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>" style="color: #fff;"><?php $date = $cricketer->created_at;
                                    echo date("d-M-Y", strtotime($date)); ?></a></em></div>
                        <span class="latest-cat"><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>">Read More</a></span>
                    </div>
                </article>
            <?php endforeach; ?>
        </div>
        <!-- // block_inner -->
    </div>


</div><!-- main_content -->