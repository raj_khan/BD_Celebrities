<div class="main_content author_list container"><!-- main_content author_list -->
    <div class="main_page col-md-8"><!-- main_page -->
        <div class="post_header"><!-- post_header -->
            <h1>About</h1>
            <span class="title_divider"></span>
        </div><!-- // post_header -->

        <div class="main_author"><!-- main_author -->
            <div class="authorInfo"><!-- authorInfo -->
                <h4 class="authorname">About</h4>
                <p class="authordescrption">
                    BD Celebrities is one of the largest bangladeshi celebrities bio based magazine in Bangladesh. we created to unfold the real story of bangladeshi celebrities (cricketers, singers, actress, youtubers, doctors, politician etc.) and to serve
                    information about them to the readers.

</p>
            </div><!-- // authorInfo -->

            <br>
            <div class="authorInfo"><!-- authorInfo -->
                <h4 class="authorname">Our Contact Address:</h4>
                <p class="authordescrption">
                    PA/136/4/1 (Level 5) <br>
                    South Badda, Gulshan <br>
                    Dhaka 1212 <br>
                    Bangladesh <br>
                    Email us: bdscelebrities@gmail.com <br></p>
            </div><!-- // authorInfo -->
        </div><!-- // main_author -->


    </div><!-- // main_page -->

    <div class="sidebar col-md-4"><!--Start Sidebar -->
        <div class="row">
            <div class="inner_sidebar"><!-- Start inner_sidebar -->


                <div class="widget widget_social_counter"> <!-- widget_social_counter -->
                    <h4 class="widget_title">Follow Us</h4>
                    <div class="social_counter social_counter_twitter">
                        <a class="social_counter_icon" href="https://twitter.com/BDCelebrities1/"
                           title="Follow our twitter" target="_blank"><i class="fa fa-twitter"></i></a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_facebook">
                        <a class="social_counter_icon" href="https://www.facebook.com/bdcelebritiesmagazin/"
                           title="Like our facebook" target="_blank"><i class="fa fa-facebook"></i></a>
<!--                        <div class="social_counter_counter">-->
<!--                            <div class="social_counter_count">67.8k</div>-->
<!--                            <div class="social_counter_unit">Fans</div>-->
<!--                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_instagram">
                        <a class="social_counter_icon" href="https://www.pinterest.com/bdcelebrities/"
                           title="Follow our instagram" target="_blank"><i class="fa fa-pinterest"></i></a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_youtube">
                        <a class="social_counter_icon" href="https://www.youtube.com/channel/UCaFFb9d88q0K7LqsXh7hqnA"
                           title="Subscribe our youtube" target="_blank"><i class="fa fa-youtube"></i></a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_googleplus">
                        <a class="social_counter_icon" href="https://plus.google.com/u/3/108974769044226584058?tab=wX"
                           title="+1 our page" target="_blank"><i class="fa fa-google-plus"></i></a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_soundcloud">
                        <a class="social_counter_icon" href="https://www.linkedin.com/company/14451949"
                           title="Subscribe our page" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <div class="social_counter_counter">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- End widget_social_counter -->


                <div class="widget widget_recent_post"><!-- Start widget recent post -->
                    <h4 class="widget_title">Popular Celebrities</h4>
                    <ul class="recent_post">

                        <?php $actress = $this->Bio_model->view_actress_bio_for_home(); ?>

                        <?php
                        foreach ($actress as $actor):
                            ?>

                            <li>
                                <figure class="widget_post_thumbnail">
                                    <a href="#"><img
                                                src="<?php echo base_url(); ?>upload/actress/<?php echo $actor->img; ?>"
                                                height="80" width="80" alt="<?php echo $actor->name; ?>"></a>
                                </figure>
                                <div class="widget_post_info">
                                    <h5><a href="#"><?php echo $actor->name; ?></a></h5>
                                    <div class="post_meta">
                                        <span class="date_meta"><a href="#"><i
                                                        class="fa fa-calendar"></i><?php $date = $actor->created_at;
                                                echo date("d-M-Y", strtotime($date)); ?></a></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>

                        <?php $singers = $this->Bio_model->view_singers_for_home(); ?>

                        <?php
                        foreach ($singers as $singer):
                            ?>

                            <li>
                                <figure class="widget_post_thumbnail">
                                    <a href="#"><img
                                                src="<?php echo base_url(); ?>upload/singers/<?php echo $singer->img; ?>"
                                                height="80" width="80" alt="<?php echo $singer->name; ?>"></a>
                                </figure>
                                <div class="widget_post_info">
                                    <h5><a href="#"><?php echo $singer->name; ?></a></h5>
                                    <div class="post_meta">
                                        <span class="date_meta"><a href="#"><i
                                                        class="fa fa-calendar"></i><?php $date = $singer->created_at;
                                                echo date("d-M-Y", strtotime($date)); ?></a></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>

                        <?php $cricketers = $this->Bio_model->view_cricketers_for_home(); ?>

                        <?php
                        foreach ($cricketers as $cricketer):
                            ?>

                            <li>
                                <figure class="widget_post_thumbnail">
                                    <a href="#"><img
                                                src="<?php echo base_url(); ?>upload/cricketers/<?php echo $cricketer->img; ?>"
                                                height="80" width="80" alt="<?php echo $cricketer->name; ?>"></a>
                                </figure>
                                <div class="widget_post_info">
                                    <h5><a href="#"><?php echo $cricketer->name; ?></a></h5>
                                    <div class="post_meta">
                                        <span class="date_meta"><a href="#"><i
                                                        class="fa fa-calendar"></i><?php $date = $cricketer->created_at;
                                                echo date("d-M-Y", strtotime($date)); ?></a></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>


                    </ul>
                </div><!-- End widget recent post -->


                <!--								-->
                <!---->
                <!--								-->
                <!--							<div class="widget widget_advertisement">-->
                <!--								<h4 class="widget_title">Advertisement</h4>-->
                <!--								<div class="ads_wid">-->
                <!--									<a href="#"><img src="img/ads-300x250.png" alt="Advertisement"></a>-->
                <!--								</div>-->
                <!--							</div>-->


            </div><!-- End inner_sidebar -->
        </div>
    </div><!--End Sidebar -->
</div><!-- // main_content -->