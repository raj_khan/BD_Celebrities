<!-- Start main_content -->
<div class="main_content container">
    <!-- product_main_content -->
    <main class="product_main_content">


        <section class="product_information">
            <div class="row">
                <div class="col-md-9 col-right">
                    <div class="col-md-7 information_entry">
                        <div class="product_preview_box">
                            <ul class="product_preview_slider">
                                <li class="product_preview_item">
                                    <img class="gallery_image"
                                         src="<?php echo base_url(); ?>upload/actress/<?php echo $actress_details->img; ?>"
                                         alt=""/>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5 information_entry">
                        <div class="product_detail_box">
                            <div class="product_header">
                                <div class="product_title">
                                    <h3><?php echo $actress_details->name; ?></h3>
                                </div>
                                <div class="product_rating">
                                    <div class="ratings">
                                        <span class="star " title='Poor' data-value='1'></span>
                                        <span class="star active" title='Fair' data-value='2'></span>
                                        <span class="star active" title='Good' data-value='3'></span>
                                        <span class="star active" title='Excellent' data-value='4'></span>
                                        <span class="star active" title='WOW!!!' data-value='5'></span>
                                    </div>
                                </div>
                            </div>

                            <div class="product_desc detail_info_entry">
                                <p><?php echo $actress_details->intro; ?>.</p>
                            </div>

                        </div>
                    </div>


                    <div class="col-md-12 information_entry">
                        <div class="product-tab tab-custom">
                            <?php if (!empty($actress_details->early_life)): ?>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#introEarlyLifePersonalLife" data-toggle="tab"
                                                      aria-expanded="true">প্রাথমিক জীবন</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="introEarlyLifePersonalLife">
                                    <div class="reviews_customer_tab">
                                        <p><?php echo $actress_details->early_life; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>

                            <?php if (!empty($actress_details->personal_life)): ?>

                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#introEarlyLifePersonalLife" data-toggle="tab" aria-expanded="true">ব্যক্তিগত জীবন</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="introEarlyLifePersonalLife">
                                        <div class="reviews_customer_tab">
                                            <p> <?php echo $actress_details->personal_life; ?> </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>


                            <?php if (!empty($actress_details->career)): ?>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#introEarlyLifePersonalLife" data-toggle="tab"
                                                      aria-expanded="true">ক্যারিয়ার</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="introEarlyLifePersonalLife">
                                    <div class="reviews_customer_tab">
                                        <p><?php echo $actress_details->career; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>


                            <?php if (!empty($actress_details->filmography)): ?>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#introEarlyLifePersonalLife" data-toggle="tab"
                                                      aria-expanded="true">ফিল্মোগ্রাফি</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="introEarlyLifePersonalLife">
                                    <div class="reviews_customer_tab">
                                        <p><?php echo $actress_details->filmography; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>


                            <?php if (!empty($actress_details->awards)): ?>
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#introEarlyLifePersonalLife" data-toggle="tab"
                                                      aria-expanded="true">অ্যাওয়ার্ড</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="introEarlyLifePersonalLife">
                                    <div class="reviews_customer_tab">
                                        <p><?php echo $actress_details->awards; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>


                        </div>
                    </div>
                    <div class="fb-comments"
                         data-href="<?php echo base_url() . 'actress/' . $actress_details->id . '/' . seoUrl($actress_details->name); ?>"
                         data-numposts="5"></div>
                </div>

                <div class="col-md-3 col-left">
                    <div class="information_entry product_sidebar">

                        <div class="product_information_blocks">
                            <div class="information_entry products_list">
                                <h4 class="block_title inline_product_title">Related Actress</h4>

                                <?php $actress = $this->Bio_model->view_actress_bio_for_home(); ?>

                                <?php
                                foreach ($actress as $actor):
                                    ?>
                                    <div class="inline_product_entry">
                                        <a href="<?php echo base_url() . 'actress/' . $actor->id . '/' . seoUrl($actor->name); ?>"
                                           class="product_image"><img alt="<?php echo $actor->name; ?>"
                                                                      src="<?php echo base_url(); ?>upload/actress/<?php echo $actor->img; ?>"
                                                                      alt=""></a>
                                        <div class="content">
                                            <div class="widget_post_info">
                                                <h5>
                                                    <a href="<?php echo base_url() . 'actress/' . $actor->id . '/' . seoUrl($actor->name); ?>"><?php echo $actor->name; ?></a>
                                                </h5>
                                                <div class="post_meta">
                                                <span class="date_meta"><a
                                                            href="<?php echo base_url() . 'actress/' . $actor->id . '/' . seoUrl($actor->name); ?>"><i
                                                                class="fa fa-calendar"></i> <?php $date = $actor->created_at;
                                                        echo date("d-M-Y", strtotime($date)); ?></a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                <?php endforeach; ?>


                            </div>
                        </div>

                        <!-- Start widget Advertisement -->
                        <!--                        <div class="product_information_blocks widget_advertisement">-->
                        <!--                            <div class="ads_wid">-->
                        <!--                                <a href="#"><img src="-->
                        <?php //echo base_url(); ?><!--template/frontend/assets/img/shopping/product/sale3.png" alt="Advertisement"></a>-->
                        <!--                            </div>-->
                        <!--                        </div>-->
                        <!-- End Widget Advertisement -->
                    </div>
                </div>
            </div>
        </section>


    </main>
    <!-- // product_main_content -->
</div>
<!-- main_content -->