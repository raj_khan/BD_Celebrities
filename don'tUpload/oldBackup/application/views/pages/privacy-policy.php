<div class="main_content author_list container"><!-- main_content author_list -->
    <div class="main_page col-md-8"><!-- main_page -->
        <div class="post_header"><!-- post_header -->
            <h1>Privacy Policy</h1>
            <span class="title_divider"></span>
        </div><!-- // post_header -->

        <div class="main_author"><!-- main_author -->

            <!-- // Privacy  Policy -->
            <div class="authorInfo">
                <h4 class="authorname">Privacy Policy for BD Celebrities</h4>
                <p class="authordescrption">If you require any more information or have any questions about our privacy
                    policy, please feel free to contact us by email at info@bdcelebrities.com. At
                    http://www.bdcelebrities.com/ we consider the privacy of our visitors to be extremely
                    important. </p>
            </div>


            <!-- // What information do we collect?-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">What information do we collect?</h4>
                <p class="authordescrption">While using our Service, we may ask you to provide us with certain
                    personally identifiable information that can be used to contact or identify you ("Personal Data").
                    Personally identifiable information may include, but is not limited to:

                    <br>
                <ul>
                    <li>First name and last name</li>
                </ul>
                </p>
            </div>


            <!-- // Cookies-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">Cookies</h4>
                <p class="authordescrption">http://www.bdcelebrities.com/ does not use cookies.</p>
            </div>


            <!--             // Sharing and selling information-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">Security Of Data</h4>
                <p class="authordescrption">The security of your data is important to us, but remember that no method of
                    transmission over the Internet, or method of electronic storage is 100% secure. While we strive to
                    use commercially acceptable means to protect your Personal Data.There is no third party included
                    operating our site.</p>
            </div>


            <!-- // Google Analytics-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">Google Analytics</h4>
                <p class="authordescrption">Bdcelebrities.com uses Google Analytics to gather information about the use
                    of our site. Google Analytics is a web analytics service offered by Google that tracks and reports
                    website traffic. Google uses the data collected to track and monitor the use of our Service. This
                    data is shared with other Google services.
                    <br>
                    Google Analytics only collects your IP address on the date you visit our website, not your name or
                    other information.</p>
            </div>


            <!--             //Log Files-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">Log Files</h4>
                <p class="authordescrption">http://www.bdcelebrities.com/ makes use of log files. These files merely
                    logs visitors to the site - usually a standard procedure for hosting companies and a part of hosting
                    services's analytics. The information inside the log files includes internet protocol (IP)
                    addresses, browser type, Internet Service Provider (ISP), date/time stamp, referring/exit pages, and
                    possibly the number of clicks.</p>
            </div>

            <!-- //DoubleClick DART Cookie-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">DoubleClick DART Cookie</h4>
                <p class="authordescrption">→ Google, as a third party vendor, uses cookies to serve ads on
                    http://www.bdcelebrities.com/.
                    <br>
                    → Google's use of the DART cookie enables it to serve ads to our site's visitors based upon their
                    visit to http://www.bdcelebrities.com/ and other sites on the Internet.
                    <br>
                    → Users may opt out of the use of the DART cookie by visiting the Google ad and content network
                    privacy policy at the following URL - http://www.google.com/privacy_ads.html</p>
            </div>


            <!--             //Third Party Privacy Policies-->

            <br>
            <div class="authorInfo">
                <h4 class="authorname">Third Party Privacy Policies</h4>
                <p class="authordescrption">When you visit our website, then we allows third-party companies
                    advertisement and/or collect certain anonymous information.
                    You should consult the respective privacy policies of these third-party ad servers for more detailed
                    information on their practices as well as for instructions about how to opt-out of certain
                    practices. http://www.bdcelebrities.com/'s privacy policy does not apply to, and we cannot control
                    the activities of, such other advertisers or web sites.
                </p>
            </div>


            <!-- //Children's Information-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">Children's Information</h4>
                <p class="authordescrption">We believe it is important to provide added protection for children online.
                    We encourage parents and guardians to spend time online with their children to observe, participate
                    in and/or monitor and guide their online activity. http://www.bdcelebrities.com/ does not knowingly
                    collect any personally identifiable information from children under the age of 13.</p>
            </div>


            <!-- //Online Privacy Policy Only-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname">Online Privacy Policy Only</h4>
                <p class="authordescrption">This privacy policy applies only to our online activities and is valid for
                    visitors to our website and regarding information shared and/or collected there. This policy does
                    not apply to any information collected offline or via channels other than this website.

                </p>
            </div>


            <!-- // Consent-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname"> Consent</h4>
                <p class="authordescrption">By using our website, you hereby consent to our privacy policy and agree to
                    its terms.

                </p>
            </div>

            <!-- // Update-->
            <br>
            <div class="authorInfo">
                <h4 class="authorname"> Update</h4>
                <p class="authordescrption">This Privacy Policy was last updated on: Thursday, September 20th, 2018.
                    Should we update, amend or make any changes to our privacy policy, those changes will be posted
                    here.
                </p>
            </div>


        </div><!-- // main_author -->


    </div><!-- // main_page -->

    <div class="sidebar col-md-4"><!--Start Sidebar -->
        <div class="row">
            <div class="inner_sidebar"><!-- Start inner_sidebar -->


                <div class="widget widget_social_counter"> <!-- widget_social_counter -->
                    <h4 class="widget_title">Follow Us</h4>
                    <div class="social_counter social_counter_twitter">
                        <a class="social_counter_icon" href="https://twitter.com/BDCelebrities1/"
                           title="Follow our twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                        <div class="social_counter_counter">
                            <div class="social_counter_count">54k</div>
                            <div class="social_counter_unit">Followers</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_facebook">
                        <a class="social_counter_icon" href="https://www.facebook.com/bdcelebritiesmagazin/"
                           title="Like our facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                        <div class="social_counter_counter">
                            <div class="social_counter_count">67.8k</div>
                            <div class="social_counter_unit">Fans</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_instagram">
                        <a class="social_counter_icon" href="https://www.pinterest.com/bdcelebrities/"
                           title="Follow our instagram" target="_blank"><i class="fa fa-pinterest"></i></a>
                        <div class="social_counter_counter">
                            <div class="social_counter_count">58.4k</div>
                            <div class="social_counter_unit">Followers</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_youtube">
                        <a class="social_counter_icon" href="https://www.youtube.com/channel/UCaFFb9d88q0K7LqsXh7hqnA"
                           title="Subscribe our youtube" target="_blank"><i class="fa fa-youtube"></i></a>
                        <div class="social_counter_counter">
                            <div class="social_counter_count">5.2M</div>
                            <div class="social_counter_unit">Subscribers</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_googleplus">
                        <a class="social_counter_icon" href="https://plus.google.com/u/3/108974769044226584058?tab=wX"
                           title="+1 our page" target="_blank"><i class="fa fa-google-plus"></i></a>
                        <div class="social_counter_counter">
                            <div class="social_counter_count">10.6k</div>
                            <div class="social_counter_unit">People</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="social_counter social_counter_soundcloud">
                        <a class="social_counter_icon" href="https://www.linkedin.com/company/14451949"
                           title="Subscribe our page" target="_blank"><i class="fa fa-linkedin"></i></a>
                        <div class="social_counter_counter">
                            <div class="social_counter_count">181.8k</div>
                            <div class="social_counter_unit">Subscribers</div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- End widget_social_counter -->


                <div class="widget widget_recent_post"><!-- Start widget recent post -->
                    <h4 class="widget_title">Popular Celebrities</h4>
                    <ul class="recent_post">

                        <?php $actress = $this->Bio_model->view_actress_bio_for_home(); ?>

                        <?php
                        foreach ($actress as $actor):
                            ?>

                            <li>
                                <figure class="widget_post_thumbnail">
                                    <a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"><img
                                                src="<?php echo base_url(); ?>upload/actress/<?php echo $actor->img; ?>"
                                                height="80" width="80" alt="<?php echo $actor->name; ?>"></a>
                                </figure>
                                <div class="widget_post_info">
                                    <h5><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"><?php echo $actor->name; ?></a></h5>
                                    <div class="post_meta">
                                        <span class="date_meta"><a href="<?php echo base_url() .'actress/'. $actor->id .'/'. seoUrl($actor->name); ?>"><i class="fa fa-calendar"></i><?php $date = $actor->created_at; echo date("d-M-Y", strtotime($date)); ?></a></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>

                        <?php $singers = $this->Bio_model->view_singers_for_home(); ?>

                        <?php
                        foreach ($singers as $singer):
                            ?>

                            <li>
                                <figure class="widget_post_thumbnail">
                                    <a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"><img src="<?php echo base_url(); ?>upload/singers/<?php echo $singer->img; ?>"
                                                height="80" width="80" alt="<?php echo $singer->name; ?>"></a>
                                </figure>
                                <div class="widget_post_info">
                                    <h5><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"><?php echo $singer->name; ?></a></h5>
                                    <div class="post_meta">
                                        <span class="date_meta"><a href="<?php echo base_url() .'singer/'. $singer->id .'/'. seoUrl($singer->name); ?>"><i class="fa fa-calendar"></i><?php $date = $singer->created_at; echo date("d-M-Y", strtotime($date)); ?></a></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>

                        <?php $cricketers = $this->Bio_model->view_cricketers_for_home(); ?>

                        <?php
                        foreach ($cricketers as $cricketer):
                            ?>

                            <li>
                                <figure class="widget_post_thumbnail">
                                    <a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><img src="<?php echo base_url(); ?>upload/cricketers/<?php echo $cricketer->img; ?>" height="80" width="80" alt="<?php echo $cricketer->name; ?>"></a>
                                </figure>
                                <div class="widget_post_info">
                                    <h5><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><?php echo $cricketer->name; ?></a></h5>
                                    <div class="post_meta">
                                        <span class="date_meta"><a href="<?php echo base_url() .'cricketer/'. $cricketer->id .'/'. seoUrl($cricketer->name); ?>"><i
                                                        class="fa fa-calendar"></i><?php $date = $cricketer->created_at; echo date("d-M-Y", strtotime($date)); ?></a></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>


                    </ul>
                </div><!-- End widget recent post -->


                <!--								-->
                <!---->
                <!--								-->
                <!--							<div class="widget widget_advertisement">-->
                <!--								<h4 class="widget_title">Advertisement</h4>-->
                <!--								<div class="ads_wid">-->
                <!--									<a href="#"><img src="img/ads-300x250.png" alt="Advertisement"></a>-->
                <!--								</div>-->
                <!--							</div>-->


            </div><!-- End inner_sidebar -->
        </div>
    </div><!--End Sidebar -->
</div><!-- // main_content -->