<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin | BD Celebrities</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">


    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/plugins/select2/select2.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/css/fileinput.css">
    <!-- Favicon -->
    <link rel="icon" type="image/png" sizes="32x32" href="favicon.png">

    <script src="<?php echo base_url(); ?>template/backend/assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?php echo base_url(); ?>template/backend/assets/js/fileinput.js"></script>
    <script src="<?php echo base_url(); ?>template/backend/assets/js/fr.js"></script>
    <script src="<?php echo base_url(); ?>template/backend/assets/js/es.js"></script>
    <script src="<?php echo base_url(); ?>template/backend/assets/js/theme.js"></script>
    <script src="<?php echo base_url(); ?>template/backend/assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/backend/assets/plugins/datatables/dataTables.bootstrap.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>Admin</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="main_menu_area">

                <a href="<?php  echo base_url(); ?>Super_admin/logout"> <h4 style="float: right; padding-right: 50px; font-weight: bold; color: #fff">Logout <i class="fa fa-power-off" aria-hidden="true"></i></h4></a>


                <a href="#">
                    <h4 style="float: right; padding-right: 50px; font-weight: bold; color: #fff"><?php  echo $this->session->userdata('name'); ?> <i class="fa fa-user" aria-hidden="true"></i>

                    </h4>
                </a>

                <a href="password_change.php"><h4 style="float: right; padding-right: 50px; font-weight: bold; color: #fff"> Password Change <i class="fa fa-key"> </i>
                    </h4></a>

            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->


<!-- ==========================  SideBar ================-->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header text-center" style="color: #9c6411;">MAIN NAVIGATION</li>

                <!--======Main Slider========-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-image"></i> <span>Main Slider</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('add-main-slider'); ?>"><i class="fa fa-edit"></i>Add Main Slider</a></li>

                        <li><a href="<?php echo base_url('view-main-slider'); ?>"><i class="fa fa-eye"></i>Manage Main Slider </a></li>

                    </ul>
                </li>


                <!--====== Category========-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-sitemap"></i> <span> Category</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url(); ?>Category/add_category"><i class="fa fa-edit"></i>Add Main Category</a></li>

                        <li><a href="<?php echo base_url('view-all-category'); ?>"><i class="fa fa-eye"></i>Manage Category </a></li>
                        <li><a href="<?php echo base_url('add-celebrity-category'); ?>"><i class="fa fa-edit"></i>Add Celebrity Category</a></li>

                        <li><a href="<?php echo base_url('view-celebrity-category'); ?>"><i class="fa fa-eye"></i>Manage Celebrity Category </a></li>

                    </ul>
                </li>


                <!--====== Cricketers bio========-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-creative-commons"></i> <span> Cricketers</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('create-cricketers-bio'); ?>"><i class="fa fa-edit"></i>Add Cricketers Bio</a></li>
                        <li><a href="<?php echo base_url('view-cricketers-bio'); ?>"><i class="fa fa-eye"></i>Manage Cricketers Bio </a></li>

                    </ul>
                </li>


                <!--====== Actress bio========-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-adn"></i> <span> Actress</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('create-actress-bio'); ?>"><i class="fa fa-edit"></i>Add Actress Bio</a></li>
                        <li><a href="<?php echo base_url('view-actress-bio'); ?>"><i class="fa fa-eye"></i>Manage Actress Bio </a></li>
                    </ul>
                </li>


                <!--====== Singers bio========-->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-music"></i> <span> Singers</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo base_url('create-singers-bio'); ?>"><i class="fa fa-edit"></i>Add Singers Bio</a></li>
                        <li><a href="<?php echo base_url('view-singers-bio'); ?>"><i class="fa fa-eye"></i>Manage Singers Bio </a></li>
                    </ul>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>




<!-- =======================Content   ===================-->

<?php echo $dashboard_content; ?>



<!--==================Footer    ==========================-->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.1.00
        </div>
        <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="http://bsdbd.com/" target="blank">BSD</a>.</strong> All rights reserved.
    </footer>

    <div class="control-sidebar-bg"></div>

</div>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>template/backend/assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>template/backend/assets/dist/js/demo.js"></script>
<!---=================================Editor===================================----->
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!---=================================Data Table===================================----->
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>template/backend/assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    $(function () {
//Initialize Select2 Elements
        $(".select2").select2();
        //Initialize wysihtml5 Elements
        $(".wysihtml5").wysihtml5();

//Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
//Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
//Money Euro
        $("[data-mask]").inputmask();

//Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });

//iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass: 'iradio_minimal-blue'
        });
//Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
//Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });

//Colorpicker
        $(".my-colorpicker1").colorpicker();
//color picker with addon
        $(".my-colorpicker2").colorpicker();

//Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });


</script>



<script>

    // Counter
    $('.statistic-counter').counterUp({
        delay: 10,
        time: 2000
    });

</script>
</body>
</html>
