<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">
        <div class="row">

            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;"> Actress  Bio View</h2>
            </div>

            <!---=======================Data Table=====================------>

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Sl</th>
                                <th>Cat</th>
                                <th>Name</th>
                                <th>Intro</th>
                                <th>Early Life</th>
                                <th>P Life</th>
                                <th>Career</th>
                                <th>Discography</th>
                                <th>Awards</th>
                                <th>Img</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>



                            <?php $categories = $this->Bio_model->manage_singers_bio_info(); ?>

                            <?php $i = 1;
                            foreach ($categories as $category):
                                $mainCategories = $this->Bio_model->find_celebrities_category($category->celebrity_id);
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $mainCategories->name; ?></td>
                                    <td><?php echo $category->name; ?></td>
                                    <td><?php
                                        $substr = $category->intro;
                                        $length = strlen($substr);
                                        if ($length >40) {
                                            $substr = substr($substr, 0, 39);
                                            echo $substr . '&nbsp; <span class=text-danger>.....</span>';
                                        } else {
                                            echo $substr;
                                        }
                                        ?></td>
                                    <td><?php
                                        $substr = $category->early_life;
                                        $length = strlen($substr);
                                        if ($length >40) {
                                            $substr = substr($substr, 0, 39);
                                            echo $substr . '&nbsp; <span class=text-danger>.....</span>';
                                        } else {
                                            echo $substr;
                                        }
                                    ?></td>
                                    <td><?php
                                        $substr = $category->personal_life;
                                        $length = strlen($substr);
                                        if ($length >40) {
                                            $substr = substr($substr, 0, 39);
                                            echo $substr . '&nbsp; <span class=text-danger>.....</span>';
                                        } else {
                                            echo $substr;
                                        }
                                    ?></td>
                                    <td><?php
                                        $substr = $category->career;
                                        $length = strlen($substr);
                                        if ($length >40) {
                                            $substr = substr($substr, 0, 39);
                                            echo $substr . '&nbsp; <span class=text-danger>.....</span>';
                                        } else {
                                            echo $substr;
                                        }
                                    ?></td>
                                    <td><?php
                                        $substr = $category->discography;
                                        $length = strlen($substr);
                                        if ($length >40) {
                                            $substr = substr($substr, 0, 39);
                                            echo $substr . '&nbsp; <span class=text-danger>.....</span>';
                                        } else {
                                            echo $substr;
                                        }
                                    ?></td>
                                    <td><?php
                                        $substr = $category->awards;
                                        $length = strlen($substr);
                                        if ($length >40) {
                                            $substr = substr($substr, 0, 39);
                                            echo $substr . '&nbsp; <span class=text-danger>.....</span>';
                                        } else {
                                            echo $substr;
                                        }
                                    ?></td>
                                    <td>
                                        <img style="width:50px; height:auto;"
                                             src="upload/singers/<?php echo $category->img; ?>">
                                    </td>
                                    <td>
                                        <?php if ($category->status == 1): ?>
                                            <a href="<?php echo base_url(); ?>un-publish-singers-bio/<?php echo $category->id; ?>">
                                                <button type="button" class="btn btn-danger">Inactive</button>
                                            </a>
                                        <?php else: ?>
                                            <a href="<?php echo base_url(); ?>publish-singers-bio/<?php echo $category->id; ?>">
                                                <button type="button" class="btn btn-success">Active</button>
                                            </a>
                                        <?php endif; ?>
                                    </td>

                                    <td>
                                        <a href="<?php echo base_url(); ?>update-singers-bio/<?php echo $category->id; ?>">
                                            <button type="button" class="btn btn-success">Edit</button>
                                        </a>
                                        <a href="<?php echo base_url(); ?>delete-singers-bio/<?php echo $category->id; ?>">
                                            <button type="button" class="btn btn-danger"
                                                    onclick="return confirm('Are you sure you want to delete?')">Delete
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                <?php $i++; endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->


        </div>
        <!-- /.row -->


    </div>

</div>

<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>