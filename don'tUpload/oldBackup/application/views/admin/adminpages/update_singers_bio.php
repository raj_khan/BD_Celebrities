<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box-body">

        <div class="row">
            <div>
                <h2 class="bg-success text-primary text-center" style="font-family: monospace; font-weight: bold;"> Update Actress Bio</h2>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                  action="<?php echo base_url('Bio/update_singers_bio'); ?>">
                <div class="col-md-8 col-md-offset-2">

                    <div class="form-group">
                        <label>Celebrity Category : </label>
                        <select name="celebrity_id" class="form-control select2" style="width: 100%;">
                            <?php $category_name = $this->Bio_model->selected_celebrities_publish_category(); ?>
                            <?php foreach ($category_name as $category): ?>
                                <option value="<?php echo $singers_info->celebrity_id; ?>" <?php if ($singers_info->celebrity_id == $category->id) {
                                    echo "Selected";
                                } ?> > <?php echo $category->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label> Name : </label>
                        <input type="hidden" name="id" value="<?php echo $singers_info->id; ?>" class="form-control">
                        <input type="text" name="name" value="<?php echo $singers_info->name; ?>" class="form-control">
                    </div>

                    <label> Intro : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="intro" class="wysihtml5" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->intro; ?></textarea>

                        </div>
                    </div>


                    <label> Early Life : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="early_life" class="wysihtml5" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->early_life; ?></textarea>

                        </div>
                    </div>


                    <label> Personal Life : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="personal_life" class="wysihtml5" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->personal_life; ?></textarea>

                        </div>
                    </div>

                    <label> Career : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="career" class="wysihtml5" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->career; ?></textarea>

                        </div>
                    </div>

                    <label> Discography : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="discography" class="wysihtml5" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->discography; ?></textarea>

                        </div>
                    </div>

                    <label> Awards : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="awards" class="wysihtml5" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->awards; ?></textarea>

                        </div>
                    </div>



                    <label> Meta Keywords : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="meta_tag" class="" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->meta_tag; ?></textarea>

                        </div>
                    </div>


                    <label> Meta Description : </label>
                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body pad">

                            <textarea name="meta_description" class="" placeholder="Place some text here"
                                      style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $singers_info->meta_description; ?></textarea>

                        </div>
                    </div>




                    <div class="col-md-6 form-group">

                        <input type="hidden"   name="old"  value="<?php echo $singers_info->img  ?>">

                        <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                    </div>
                    <div class="col-md-6" style="padding-bottom: 10px;">
                        <img src="<?php echo base_url();?>upload/singers/<?php echo $singers_info->img ?>" style="height: 280px; width: 347px;">
                    </div>

                    <div class="form-group">
                        <label>Publication Status : </label>
                        <select name="status" class="form-control select2" style="width: 100%;">
                            <option value="1">PUBLISH</option>
                            <option value="0">UN-PUBLISH</option>
                        </select>
                    </div>

                    <input class="btn btn-success" type="submit" name="submit" value="Update" style="float: right">
                </div>

            </form>

        </div>
        <!-- /.row -->


    </div>

</div>