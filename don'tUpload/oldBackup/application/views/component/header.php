<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<!--> <!--<![endif]-->
<html lang="en">

<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Grid Layout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">

    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">


    <!-- Javascript
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.v2.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

</head>
<body>

<!-- Grid Layout Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="all_content animsition container-fluid">
    <div class="row">
        <div class="header"><!-- Start header -->
            <div class="top_bar"><!-- Start top_bar -->
                <div class="min_top_bar"><!-- Start min_top_bar -->
                    <div class="container">
                        <div class="top_nav"><!-- Start top_nav -->
                            <ul>
                                <li><a href="<?php echo base_url(); ?>editor">about me</a></li>
                                <li><a href="<?php echo base_url(); ?>contact">contact</a></li>
                                <li><a href="#">Pages</a></li>
                            </ul>
                        </div><!-- End top_nav -->

                        <div id="top_search_ico"><!-- Start top_search_ico -->
                            <div class="top_search"><!-- Start top_search -->
                                <form method="get"><input type="text" placeholder="Search and hit enter..."></form>
                                <i class="fa fa-search search-desktop"></i>
                            </div><!-- End top_search -->

                            <div id="top_search_toggle"><!-- Start top_search_toggle -->
                                <div id="search_toggle_top">
                                    <form method="get"><input type="text" placeholder="Search and hit enter..."></form>
                                </div>
                                <i class="fa fa-search search-desktop"></i>
                            </div><!-- End top_search_toggle -->
                        </div><!-- End top_search_ico -->

                        <div class="social_icon"><!-- Start social_icon -->
                            <span><a href="https://www.facebook.com/bdscelebrities/" target="_blank"><i class="fa fa-facebook"></i></a></span>
                            <span><a href="https://twitter.com/bd_celebrities"><i class="fa fa-twitter"></i></a></span>
                            <span><a href="#"><i class="fa fa-google-plus"></i></a></span>
                            <span><a href="#"><i class="fa fa-youtube"></i></a></span>
                            <span><a href="#"><i class="fa fa-instagram"></i></a></span>

                            <span><a href="#"><i class="fa fa-linkedin"></i></a></span>
                        </div><!-- End social_icon -->
                    </div>
                </div><!-- End min_top_bar -->
            </div><!-- End top_bar -->

            <div class="main_header"><!-- Start main_header -->
                <div class="container">

                    <div class="logo logo_blog_layout"><!-- Start logo -->
                        <!-- <h3>logo</h3> -->
                        <a href="<?php echo base_url(); ?>home"><img src="<?php echo base_url(); ?>assets/img/demo/logo.png" alt="Logo"></a>
                    </div><!-- End logo -->

                    <div class="nav_bar"><!-- Start nav_bar -->
                        <nav id="primary_nav_wrap"><!-- Start primary_nav_wrap -->
                            <ul>
                                <li class="current-menu-item"><a href="<?php echo base_url(); ?>home">Home</a></li>
                                <li><a href="<?php echo base_url(); ?>home">Cricketers</a></li>

                                <li><a href="<?php echo base_url(); ?>home">Singers</a></li>

                                <li><a href="<?php echo base_url(); ?>home">Actors</a></li>

                                <li><a href="#">Bio</a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>bio">Actors</a></li>
                                        <li><a href="<?php echo base_url(); ?>bio">Cricketers</a></li>
                                        <li><a href="<?php echo base_url(); ?>bio">Singers</a></li>
                                        <li><a href="<?php echo base_url(); ?>bio">Others</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>contact">contact</a></li>
                            </ul>
                        </nav><!-- End primary_nav_wrap -->
                    </div><!-- End nav_bar -->

                    <div class="hz_responsive"><!--button for responsive menu-->
                        <div id="dl-menu" class="dl-menuwrapper">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                <li class="current-menu-item"><a href="<?php echo base_url(); ?>home">Home</a></li>
                                <li><a href="<?php echo base_url(); ?>home">Cricketers</a></li>

                                <li><a href="<?php echo base_url(); ?>home">Singers</a></li>

                                <li><a href="<?php echo base_url(); ?>home">Actors</a></li>

                                <li><a href="#">Bio</a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>bio">Actors</a></li>
                                        <li><a href="<?php echo base_url(); ?>bio">Cricketers</a></li>
                                        <li><a href="<?php echo base_url(); ?>bio">Singers</a></li>
                                        <li><a href="<?php echo base_url(); ?>bio">Others</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>contact">contact</a></li>
                            </ul>
                        </div><!-- /dl-menuwrapper -->
                    </div><!--End button for responsive menu-->

                </div>
            </div><!-- End main_header -->
        </div><!-- End header -->