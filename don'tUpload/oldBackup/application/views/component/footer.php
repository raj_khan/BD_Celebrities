<div id="footer" class="footer container-fulid"><!-- Start footer -->
    <footer class="main_footer"><!-- Start main_footer -->
        <div class="container">
            <div class="row">

                <div class="col-sm-4"><!-- Start widget_text -->
                    <div id="text-3" class="widget widget_text">
                        <h4 class="widget_title">Text Widget</h4>
                        <div class="textwidget">
                            Here you can add any text or Here you can add any text or html snippet. Good for showing some small info or about your site. html snippet. Good for showing info or about your site.
                        </div>
                    </div>
                </div><!-- End widget_text -->

                <div class="col-sm-4"><!-- Start widget tag cloud -->
                    <div class="widget  widget_tag_cloud">
                        <h4 class="widget_title">Tags</h4>
                        <div class="tagcloud">
                            <a href="#">audio</a>
                            <a href="#">dailymotion</a>
                            <a href="#">Gallery</a>
                            <a href="#">LightBox</a>
                            <a href="#">Link</a>
                            <a href="#">mp3</a>
                            <a href="#">nature</a>
                            <a href="#">post</a>
                            <a href="#">Quote</a>
                            <a href="#">slider</a>
                            <a href="#">soundcloud</a>
                            <a href="#">sport</a>
                            <a href="#">Standard</a>
                            <a href="#">Twitter</a>
                            <a href="#">vimeo</a>
                        </div>
                    </div>
                </div><!-- End widget tag cloud -->

                <div class="col-sm-4"><!-- Start widget flickr -->
                    <div class="widget hamzh_flickr">
                        <h4 class="widget_title">Flickr Photos</h4>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?><?php echo base_url(); ?>assets/img/demo/flickr/1.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/2.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/3.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>


                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/4.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/5.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/6.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/7.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                        <div class="flickr_badge_image">
                            <a href="#">
                                <img src="<?php echo base_url(); ?>assets/img/demo/flickr/8.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
                            </a>
                        </div>

                    </div>
                </div><!-- Start widget flickr -->
            </div>
        </div>
    </footer><!-- End main_footer -->

    <div class="copyright"> <!-- Start copyright -->
        <div class="hmztop">Scroll To Top</div><!-- Back top -->
        <div class="footer_logo"><!-- Start footer logo -->
            <a href="#"><img src="<?php echo base_url(); ?>assets/img/demo/footer_blog_logo.png" alt="Logo"></a>
        </div><!-- End footer logo -->
        <div class="social_icon"><!--Start social_icon -->
            <span><a href="#"><i class="fa fa-facebook"></i></a></span>
            <span><a href="#"><i class="fa fa-twitter"></i></a></span>
            <span><a href="#"><i class="fa fa-google-plus"></i></a></span>
            <span><a href="#"><i class="fa fa-youtube"></i></a></span>
            <span><a href="#"><i class="fa fa-dribbble"></i></a></span>
            <span><a href="#"><i class="fa fa-behance"></i></a></span>
            <span><a href="#"><i class="fa fa-instagram"></i></a></span>
            <span><a href="#"><i class="fa fa-vimeo-square"></i></a></span>
            <span><a href="#"><i class="fa fa-skype"></i></a></span>
            <span><a href="#"><i class="fa fa-linkedin"></i></a></span>
        </div><!--End social_icon -->
        <p>Copyrights © 2015 All Rights Reserved by <a href="#">Hamzh</a></p>
    </div><!-- End copyright -->
</div><!-- End footer -->
</div><!-- End row -->
</div><!-- End all_content -->

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Javascript
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/isotope.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.jribbble-1.0.1.ugly.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/hamzh.js"></script>
</body>

</html>
