<?php

include_once("Database.php");

class TeamMemberClass extends Database{
    
    public $con;

    public function __construct() {
        
        $this->con = parent::connect();
    }
    public function insert($postData, $fileData) {
        
        $prod_catagory = $postData['prod_catogory'];
        $Name = $postData['add_name'];
        $Designation= $postData['add_designation'];
        $FbLink = $postData['add_fb_link'];
        $TwLink = $postData['add_twiter_link'];
        $InsLink = $postData['add_instagram_link'];
        
        $Designation = mysqli_real_escape_string($this->con, $Designation);
        
        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = "../img/all-images/" . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        $product_insert_query = "INSERT INTO `team_member` (`id`, `category`, `name`, `designaton`, `fb_link`, `twiter_link`, `instagram_link`, `img_link`) VALUES (NULL, '$prod_catagory', '$Name', '$Designation', '$FbLink', '$TwLink', '$InsLink', '$file_name')";

        $insert_query = mysqli_query($this->con, $product_insert_query);
        if ($insert_query && $upload) {

            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }
    public function showTeamMember() {
        
        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `team_member` WHERE `category` = 6 ORDER BY `team_member`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       while($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)){
           if($i > 11){
               break;
           }
           $returnData[] = $row;
           $i ++;
       } 
          return $returnData;    
    }
    public function showSalesMan() {
        
        $i = 0;
        $returnData = [];
        $sql_query = 'SELECT * FROM `team_member` WHERE `category` = 7 ORDER BY `team_member`.`id` DESC';
        $queryExecute = mysqli_query($this->con,$sql_query);
        
       while($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)){
           if($i > 11){
               break;
           }
           $returnData[] = $row;
           $i ++;
       } 
          return $returnData;    
    }
    public function showForEditTeam() {
        
        $sql_query = 'SELECT * FROM `team_member` ORDER BY `team_member`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
            
        }

        return $returnData;
    }
        public function showTeamCategory($cat_id) {

            $queryForCategoryName = "SELECT * FROM `category_id_name` WHERE `catagory_id` = " . $cat_id . "";
            $CategoryName = mysqli_query($this->con, $queryForCategoryName);
             $showCategoryName= mysqli_fetch_array($CategoryName, MYSQLI_ASSOC);

            return $showCategoryName['catagory_name'];
        
    }
    public function showSingleTeam($id) {
        
         $queryForSingleTeam = "SELECT * FROM `team_member` WHERE `id` = '$id'";
        $SingleTeam = mysqli_query($this->con, $queryForSingleTeam);
        $returnData = mysqli_fetch_array($SingleTeam, MYSQLI_ASSOC);

        return $returnData;   
        
        
    }
    public function updateTeam($postData, $fileData) {
        
        $prod_catagory = $postData['prod_catogory'];
        $Name = $postData['add_name'];
        $Designation= $postData['add_designation'];
        $FbLink = $postData['add_fb_link'];
        $TwLink = $postData['add_twiter_link'];
        $InsLink = $postData['add_instagram_link'];
        $id = $postData['id'];
         
        $Designation = mysqli_real_escape_string($this->con, $Designation);
        
        $singleTeamMember = $this->showSingleTeam($id);
        
        if ($prod_catagory == "") {
            $error = "Please input Category";
        }if ($Name == "") {
            $error = "Please input Name";
        } else if ($fileData['file']['size'] > 250000) {
            $error = "Please insert image not more than 250 KB";
         }
        
        if (isset($fileData['file']['name']) && !empty($fileData['file']['name'])) {
            $file_name = uniqid() . $fileData['file']['name'];
            $destination = "../img/all-images/" . $file_name;
            $upload = move_uploaded_file($fileData['file']['tmp_name'], $destination);
        } else {
            $file_name = $singleTeamMember['img_link'];
        }
        
        $product_update_query = "UPDATE `team_member` SET`category` = '$prod_catagory', `name` = '$Name', `designaton` = '$Designation', `fb_link` = '$FbLink', `twiter_link` = '$TwLink', `instagram_link` = '$InsLink', `img_link` = '$file_name' WHERE `team_member`.`id` = $id";
        
        
        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());    
        if ($update_query) {
        ?>
        <script>
            window.location = "viewTeamMember.php";
        </script>
        <?php
    } else {
        $error = "Error! Could not Upload your Information";
        
    }
        return $update_query;
    }
}
