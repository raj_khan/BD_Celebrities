<?php

include_once("Database.php");

class AboutClass extends Database {

    public $con;

    public function __construct() {

        $this->con = parent::connect();
    }

    public function insertAbout() {

        $catagory = ($_POST['prod_catagory']);
        $discription = ($_POST['discription']);

        $discription = mysqli_real_escape_string($this->con, $discription);

        $product_insert_query = "INSERT INTO `notice` (`id`, `category`, `description`) VALUES (NULL, '$catagory', '$discription')";

        $insert_query = mysqli_query($this->con, $product_insert_query);
        if ($insert_query) {
            return "Success! Upload your information ";
        } else {
            return "Error! Could not Upload your Information";
        }
    }

    public function showWelcomeMessage() {

        $returnData = '';
        $sql_query = 'SELECT * FROM `notice` WHERE `category` = 3 ORDER BY `notice`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);

        $returnData = $row;

        return $returnData;
    }

    public function showWhyChooseMessage() {

        $returnData = '';
        $sql_query = 'SELECT * FROM `notice` WHERE `category` = 4 ORDER BY `notice`.`id` DESC';
        $queryExecute = mysqli_query($this->con, $sql_query);

        $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);

        $returnData = $row;

        return $returnData;
    }

    public function showForEditAbout() {
        $returnData = [];
        $sql_query = 'SELECT * FROM `notice` WHERE `category` = 3';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }

        return $returnData;
    }

    public function showForEditChooseMessage() {

        $sql_query = 'SELECT * FROM `notice` WHERE `category` = 4';
        $queryExecute = mysqli_query($this->con, $sql_query);
        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {

            $returnData[] = $row;
        }

        return $returnData;
    }

    public function showSingleWelcome($id) {

        $queryForSingleWlcome = "SELECT * FROM `notice` WHERE `id` = $id";
        $singleWelcome = mysqli_query($this->con, $queryForSingleWlcome);
        $returnData = mysqli_fetch_array($singleWelcome, MYSQLI_ASSOC);

        return $returnData;
    }

    public function updateWelcome($postData) {

        $description = $postData['description'];
        $id = $postData['id'];
        $prod_title = mysqli_real_escape_string($this->con, $description);

        $singleNotice = $this->showSingleWelcome($id);

        if ($description == "") {
            $error = "Please input Welcome Text";
            return $error;
        }

        $product_update_query = "UPDATE `notice` SET `description` = '$description' WHERE `notice`.`id` = $id";

        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());
        if ($update_query) {
            return 1;
        } else {
            $error = "Error! Could not Upload your Information";
            return $error;
        }
        return $update_query;
    }

    public function updateChoose($postData) {

        $description = $postData['description'];
        $id = $postData['id'];
        $description = mysqli_real_escape_string($this->con, $description);

        if ($description == "") {
            $error = "Please input Choose Text";
            return $error;
        }

        $product_update_query = "UPDATE `notice` SET `description` = '$description' WHERE `notice`.`id` = $id";

        $update_query = mysqli_query($this->con, $product_update_query) or die(mysql_error());
        if ($update_query) {
            return 1;
        } else {
            $error = "Error! Could not Upload your Information";
            return $error;
        }
        return $update_query;
    }

}
