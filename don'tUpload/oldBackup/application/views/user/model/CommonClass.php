<?php

include_once("Database.php");

class CommonClass extends Database {

    public $con;

    public function __construct() {

        $this->con = parent::connect();
    }

    public function deleteData($table_name, $condition) {
        $query = "DELETE FROM `$table_name` WHERE $condition";
        $delete_data = mysqli_query($this->con, $query);
        if ($delete_data) {
            return true;
        } else {
            return false;
        }
    }
	
	// View All Data Function
    public function view_all($table_name) {
        $returnData = array();
        $sql = "SELECT * FROM $table_name";

        $queryExecute = mysqli_query($this->con, $sql);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            $returnData[] = $row;
        }
        return $returnData;
    }
	
	// View All Data Condition wise Function
    public function view_all_by_cond($table_name, $where_cond) {
        
        $returnData = array();
        $sql = "SELECT * FROM $table_name WHERE $where_cond";

        $queryExecute = mysqli_query($this->con, $sql);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            $returnData[] = $row;
        }
        return $returnData;
    }
	
	// Details Data View Condition Wise Function
    public function details_by_cond($table_name, $where_cond) {

        $sql = "SELECT * FROM $table_name WHERE $where_cond";
        $queryExecute = mysqli_query($this->con, $sql);

        $row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC);

        return $row;
    }
	
	 // View All Data for  product table 
    public function view_all_without_cond_2($table_name) {
        
        $returnData = array();
        $sql = "SELECT * FROM $table_name" ;
       
        $queryExecute = mysqli_query($this->con, $sql);

        while ($row = mysqli_fetch_array($queryExecute, MYSQLI_ASSOC)) {
            $returnData[] = $row;
        }
        return $returnData;
    }
	
	public function Update_data($table_name, $form_data, $where_clause = '') {

        $whereSQL = '';
        if (!empty($where_clause)) {

            if (substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE') {

                $whereSQL = " WHERE " . $where_clause;
            } else {
                $whereSQL = " " . trim($where_clause);
            }
        }

        $sql = "UPDATE " . $table_name . " SET ";

        $sets = array();
        foreach ($form_data as $column => $value) {
            $sets[] = "`" . $column . "` = '" . $value . "'";
        }
        $sql .= implode(', ', $sets);

        $sql .= $whereSQL;
        
        $q = $this->con->prepare($sql);

        return $q->execute() or die(print_r($q->errorInfo()));
    }
	
	public function file_upload($fileData, $destination_folder) {

        $file_name = uniqid() . ($fileData['file']['name']);
        $tmp_name = ($fileData['file']['tmp_name']);
        $destination = $destination_folder . $file_name;
        $upload = move_uploaded_file($tmp_name, $destination);

        return $file_name;
    }

}
