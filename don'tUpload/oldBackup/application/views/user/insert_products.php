<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

if (!isset($_SESSION["s_id"])) {
    header("location:login/login.php");
}
include_once 'model/CommonClass.php';
include 'model/Slider.php';

$model = new CommonClass();
$slider = new Slider();

include './component/header.php';
include './component/sidebar.php';


$viewName = $model->view_all_by_cond('categories', 'id >= 7 AND id <= 9'  );

if (isset($_POST['submit'])) {
    $error = $slider->insertProducts($_POST, $_FILES);
}
?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="box-body">

            <div class="row">
                <div>
                    <h2 class="bg-success text-primary text-center"
                        style="font-family: monospace; font-weight: bold;"><?php echo isset($error) ? $error : 'Insert Products'; ?> </h2>
                </div>
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <label>Select Category : </label>
                            <select name="category" class="form-control select2" style="width: 100%;">
                                <option></option>
                                <?php foreach ($viewName as $view) { ?>

                                    <option value="<?php echo $view['id']; ?>"><?php echo $view['category_name']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Product  Title : </label>
                            <input type="text" name="productTitle" class="form-control" placeholder="Enter  Title.....">
                        </div>
                        <label>Product Description : </label>
                        <div class="box">
                            <!-- /.box-header -->
                            <div class="box-body pad">
                                <form>
                                    <textarea name="productDescription" class="wysihtml5" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                </form>
                            </div>
                        </div>

                        <div class="form-group">
                            <input id="file-4" type="file" name="file" class="file" data-upload-url="#">
                        </div>

                        <input type="checkbox" name="popularStatus" value="1" > Popular Product<br>

                        <input class="btn btn-success" type="submit" name="submit" value="Submitt" style="float: right">
                    </div>

                </form>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <!-- /.content-wrapper -->
    <script>
        $("#file-4").fileinput({
            allowedFileExtensions: ['jpg', 'JPEG', 'png', 'gif'],
            showUpload: false,
            /*initialPreview: [
                "http://lorempixel.com/1920/1080/transport/1",
            ],*/
            initialPreviewConfig: [
                {caption: "transport-1.jpg", size: 329892, width: "220px", url: "{$url}", key: 1},
            ]


        });
    </script>
<?php
include './component/footer.php';
?>