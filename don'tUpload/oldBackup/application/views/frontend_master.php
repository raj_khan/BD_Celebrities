<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<!--> <!--<![endif]-->
<html lang="en">

<head>

    <!-- Basic Page Needs
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <!--<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <!--Website SEO-->
    <title><?php echo isset($title) ? $title : 'BD Celebrities'; ?></title>
    <meta name="keywords"
          content="<?php echo isset($meta_tag) ? $meta_tag : 'bd celebrities biography,bangladesh celebrities biography,bangladesh celebrities bangla biography,bangladeshi celebrities photo, bd actress biography, bd singers biography, bd cricketers biography, bd youtubers biography, bd popular business man biography, bd top politician biography, bd educational leader biography, bd top doctors biography, bd celebrities Photo'; ?>">
    <meta name="description"
          content="<?php echo isset($meta_description) ? $meta_description : 'bd celebrities biography,bangladesh celebrities biography,bangladesh celebrities bangla biography,bangladeshi celebrities photo, bd actress biography, bd singers biography, bd cricketers biography, bd youtubers biography, bd popular business man biography, bd top politician biography, bd educational leader biography, bd top doctors biography, bd celebrities Photo'; ?>">

    <meta name="robots" content="noindex, nofollow">
    <!--for fb -->

    <meta property="og:title"
          content="<?php echo isset($title) ? $title : 'BD Celebrities'; ?>"/>
    <meta property="og:site_name" content="BD Celebrities"/>
    <meta property="og:description" content="<?php echo isset($description) ? $description : 'BD Celebrities | bangladeshi celebrities  biography | bangladeshi celebrities photo'; ?>"/>

    <meta property="og:type" content="website"/>

    <meta property="og:url" content="<?php echo isset($currentUrl) ? $currentUrl : '' ?>"/>
    <meta property="og:image" content='<?php echo base_url().'/upload/'. isset ($img) ? $img : "  template/frontend/assets/img/demo/logo.png" ?> '/>
    <meta property="og:image:width" content="300"/>
    <meta property="og:image:height" content="240"/>

    <!--    For Twitter-->
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@BDCelebrities1"/>
    <meta name="twitter:url" content="https://twitter.com/BDCelebrities1/"/>
    <meta name="twitter:title"
          content="bangladesh celebrities bangla biography/bio/activity  - bd celebrities biography,bangladeshi celebrities biography,bangladeshi celebrities bangla biography,bangladeshi celebrities photo, bd actress biography, bd singers biography, bd cricketers biography, bd youtubers biography, bd popular business man biography, bd top politician biography, bd educational leader biography, bd top doctors biography, bd celebrities Photo"/>

    <meta name="twitter:image" content="<?php echo isset($img) ? $img : '  template/frontend/assets/img/demo/logo.png' ?>"/>


    <!--canonical link-->
    <link rel="canonical" href="http://www.bdcelebrities.com/">
    <meta property="og:url" content="http://www.bdcelebrities.com/">


    <!-- Mobile Specific Metas
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!---------------------------
    Facebook Commnet
    -------------------------->
    <meta property="fb:app_id" content="328027154622810" />
    <!--    <meta property="fb:admins" content="{100008455830576}">-->
    <!--    admin 2-->
    <!--    <meta property="fb:admins" content="{100011777321568}">-->

    <!-- CSS
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>template/frontend/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>template/frontend/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>template/frontend/assets/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>template/frontend/assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url(); ?>template/frontend/assets/css/shopping-page.css">


    <!-- Favicon
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>template/frontend/assets/img/favicon.png">


    <!-- Javascript
    –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.v2.1.3.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/bootstrap.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126029993-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-126029993-1');
    </script>


</head>
<!--<body oncontextmenu="return false">-->
<body class="prevent_copy"  oncontextmenu="return false">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.1&appId=328027154622810&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<!-- Grid Layout Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<div class="all_content animsition container-fluid">
    <div class="row">
        <div class="header"><!-- Start header -->
            <div class="top_bar"><!-- Start top_bar -->
                <div class="min_top_bar"><!-- Start min_top_bar -->
                    <div class="container">
                        <div class="top_nav"><!-- Start top_nav -->
                            <ul>
                                <li><a href="<?php echo base_url(); ?>about">About</a></li>
                                <li><a href="<?php echo base_url(); ?>copyright">Copyright</a></li>
                                <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>

                            </ul>
                        </div><!-- End top_nav -->

                        <div id="top_search_ico"><!-- Start top_search_ico -->
                            <div class="top_search"><!-- Start top_search -->
                                <form method="get"><input type="text"
                                                          placeholder="Type Celebrity Name and hit enter..."></form>
                                <i class="fa fa-search search-desktop"></i>
                            </div><!-- End top_search -->

                            <div id="top_search_toggle"><!-- Start top_search_toggle -->
                                <div id="search_toggle_top">
                                    <form method="get"><input type="text"
                                                              placeholder="Type Celebrity Name and hit enter..."></form>
                                </div>
                                <i class="fa fa-search search-desktop"></i>
                            </div><!-- End top_search_toggle -->
                        </div><!-- End top_search_ico -->

                        <div class="social_icon"><!-- Start social_icon -->
                            <span><a href="https://www.facebook.com/bdcelebritiesmagazin/" target="_blank"><i
                                            class="fa fa-facebook"></i></a></span>
                            <span><a href="https://twitter.com/BDCelebrities1/" target="_blank"><i
                                            class="fa fa-twitter"></i></a></span>
                            <span><a href="https://plus.google.com/u/3/108974769044226584058?tab=wX" target="_blank"><i
                                            class="fa fa-google-plus"></i></a></span>
                            <span><a href="https://www.youtube.com/channel/UCaFFb9d88q0K7LqsXh7hqnA" target="_blank"><i
                                            class="fa fa-youtube"></i></a></span>
                            <span><a href="https://www.linkedin.com/company/14451949" target="_blank"><i
                                            class="fa fa-linkedin"></i></a></span>
                            <span><a href="https://www.pinterest.com/bdcelebrities/" target="_blank"><i
                                            class="fa fa-pinterest"></i></a></span>
                        </div><!-- End social_icon -->
                    </div>
                </div><!-- End min_top_bar -->
            </div><!-- End top_bar -->

            <div class="main_header"><!-- Start main_header -->
                <div class="container">

                    <div class="logo logo_blog_layout"><!-- Start logo -->
                        <!-- <h3>logo</h3> -->
                        <a href="<?php echo base_url(); ?>home"><img
                                    src="<?php echo base_url(); ?>template/frontend/assets/img/demo/logo.png"
                                    alt="Logo"></a>
                    </div><!-- End logo -->


                    <div class="nav_bar"><!-- Start nav_bar -->
                        <nav id="primary_nav_wrap"><!-- Start primary_nav_wrap -->
                            <ul>
                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "home") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>home">HOME</a></li>
                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "popular-bangladeshi-cricketers") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>popular-bangladeshi-cricketers">CRICKETERS</a>
                                </li>

                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "popular-bangladeshi-singers") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>popular-bangladeshi-singers">SINGERS</a></li>

                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "popular-bangladeshi-actress") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>popular-bangladeshi-actress">ACTORS</a></li>

                                <li>
                                    <a class="<?php echo (((basename($_SERVER['PHP_SELF']) == "top-bangladeshi-actress-bio")) || ((basename($_SERVER['PHP_SELF']) == "top-bangladeshi-singers-bio")) || ((basename($_SERVER['PHP_SELF']) == "top-bangladeshi-cricketers-bio"))) ? "current-menu-item" : ""; ?>"
                                       href="#">BIOGRAPHY</a>
                                    <ul>
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-actress-bio">ACTRESS</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>top-bangladeshi-cricketers-bio">CRICKETERS</a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-singers-bio">SINGERS</a>
                                        </li>
                                    </ul>
                                </li>
                                <!--                                <li><a class="-->
                                <?php //echo (basename($_SERVER['PHP_SELF']) == "contact") ? "current-menu-item" : ""; ?><!--" href="-->
                                <?php //echo base_url(); ?><!--contact">CONTACT</a></li>-->
                            </ul>
                        </nav><!-- End primary_nav_wrap -->
                    </div><!-- End nav_bar -->

                    <div class="hz_responsive"><!--button for responsive menu-->
                        <div id="dl-menu" class="dl-menuwrapper">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "home") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>home">Home</a></li>
                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "popular-bangladeshi-cricketers") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>popular-bangladeshi-cricketers">cricketers</a>
                                </li>

                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "popular-bangladeshi-singers") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>popular-bangladeshi-singers">Singers</a></li>

                                <li>
                                    <a class="<?php echo (basename($_SERVER['PHP_SELF']) == "popular-bangladeshi-actress") ? "current-menu-item" : ""; ?>"
                                       href="<?php echo base_url(); ?>popular-bangladeshi-actress">Actors</a></li>

                                <li>
                                    <a class="<?php echo (((basename($_SERVER['PHP_SELF']) == "top-bangladeshi-actress-bio")) || ((basename($_SERVER['PHP_SELF']) == "top-bangladeshi-singers-bio")) || ((basename($_SERVER['PHP_SELF']) == "top-bangladeshi-cricketers-bio"))) ? "current-menu-item" : ""; ?>"
                                       href="#">Biography</a>
                                    <ul class="dl-submenu">
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-actress-bio">Actors</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url(); ?>top-bangladeshi-cricketers-bio">Cricketers</a>
                                        </li>
                                        <li><a href="<?php echo base_url(); ?>top-bangladeshi-singers-bio">Singers</a>
                                        </li>
                                    </ul>
                                </li>
                                <!--                                <li><a class="-->
                                <?php //echo (basename($_SERVER['PHP_SELF']) == "contact") ? "current-menu-item" : ""; ?><!--" href="-->
                                <?php //echo base_url(); ?><!--contact">contact</a></li>-->
                            </ul>
                        </div><!-- /dl-menuwrapper -->
                    </div><!--End button for responsive menu-->

                </div>
            </div><!-- End main_header -->
        </div><!-- End header -->

        <?php echo $main_content; ?>

        <div id="footer" class="footer container-fulid"><!-- Start footer -->

            <div class="copyright"> <!-- Start copyright -->
                <div style="background-image: url(<?php echo base_url(); ?>template/frontend/assets/img/arrow.png);"
                     class="hmztop">Scroll To Top
                </div><!-- Back top -->

                <ul style="list-style: none; display: block;">
                    <li style="display: inline-block;"><a href="<?php echo base_url(); ?>about">About</a></li>
                    <li><a href="<?php echo base_url(); ?>copyright">Copyright</a></li>
                    <li><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>

                </ul>

                <div class="social_icon"><!--Start social_icon -->
                    <span><a href="https://www.facebook.com/bdcelebritiesmagazin/" target="_blank"><i
                                    class="fa fa-facebook"></i></a></span>
                    <span><a href="https://twitter.com/BDCelebrities1/" target="_blank"><i
                                    class="fa fa-twitter"></i></a></span>
                    <span><a href="https://plus.google.com/u/3/108974769044226584058?tab=wX" target="_blank"><i
                                    class="fa fa-google-plus"></i></a></span>
                    <span><a href="https://www.youtube.com/channel/UCaFFb9d88q0K7LqsXh7hqnA" target="_blank"><i
                                    class="fa fa-youtube"></i></a></span>
                    <span><a href="https://www.linkedin.com/company/14451949" target="_blank"><i
                                    class="fa fa-linkedin"></i></a></span>
                    <span><a href="https://www.pinterest.com/bdcelebrities/" target="_blank"><i
                                    class="fa fa-pinterest"></i></a></span>
                </div><!--End social_icon -->
                <p>Copyrights © <?php echo date("Y"); ?> All Rights Reserved by <a href="<?php echo base_url(); ?> ">BD
                        Celebrities</a></p>

            </div><!-- End copyright -->

        </div><!-- End footer -->
    </div><!-- End row -->
</div><!-- End all_content -->

<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->

<!-- Javascript
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/modernizr.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/isotope.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.jribbble-1.0.1.ugly.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript"
        src="<?php echo base_url(); ?>template/frontend/assets/js/jquery.jplayer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/hamzh.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>template/frontend/assets/js/shopping_page.js"></script>


</body>

</html>
