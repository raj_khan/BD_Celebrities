
			
			<div class="main_content container">
				<div class="mian_slider clearfix"><!--Start Mian slider -->

					<div class="big_silder col-md-8"><!-- Start big silder -->
						<div class="row">
							<ul id="big-slid-post" class="a-post-box">
								<li>
									<div class="feat-item img-section" data-bg-img="assets/imgdemo/slid/1.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="#" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/imgdemo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>
								<li>
									<div class="feat-item img-section" data-bg-img="assets/imgdemo/slid/2.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="#" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/imgdemo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>
								<li>
									<div class="feat-item img-section" data-bg-img="assets/imgdemo/slid/3.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="#" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/imgdemo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>
								<li>
									<div class="feat-item img-section" data-bg-img="assets/imgdemo/slid/4.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="#" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/imgdemo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div><!-- End big silder -->

					<div class="post_box col-md-4"><!--Start Post box -->
						<div class="row">
							<article class="a-post-box">
									<figure class="latest-img"><img src="assets/imgdemo/slid/2.png" class="latest-cover" alt="If you are going to use a passage of Lorem Ipsum"></figure>
									<div class="latest-overlay"></div>
									<div class="latest-txt">
										<h4 class="latest-title"><a data-dummy="dfsdfsdfdsf" href="#" rel="bookmark" title="If you are going to use a passage of Lorem Ipsum">If you are going to use a passage of Lorem Ipsum</a></h4>
										<span class="latest-cat"><a href="#">Life</a></span>
										<span class="latest-meta">
											<span class="latest-date"><i class="fa fa-clock-o"></i>26-5-2014</span>
										</span>
									</div>
					    	</article>

							<article class="a-post-box">
									<figure class="latest-img"><img src="assets/imgdemo/slid/4.png" class="latest-cover" alt="If you are going to use a passage of Lorem Ipsum"></figure>
									<div class="latest-overlay"></div>
									<div class="latest-txt">
										<h4 class="latest-title"><a data-dummy="dfsdfsdfdsf" href="#" rel="bookmark" title="If you are going to use a passage of Lorem Ipsum">going to use a passage of Lorem</a></h4>
										<span class="latest-cat"><a href="#">Life</a></span>
										<span class="latest-meta">
											<span class="latest-date"><i class="fa fa-clock-o"></i>26-5-2014</span>
										</span>
									</div>
					    	</article>
						</div>
					</div><!--End Post box -->
				</div><!--End Mian slider -->

				<div class="posts_sidebar layout_right_sidebar clearfix"><!-- Start posts sidebar -->

					<div class="inner_single col-md-8"> <!-- Start inner single -->
						<div class="row">	
									
							<article class="hz_post"><!-- Start article -->
								<div class="video_post post_wrapper">

									<div class="hz_thumb_post">
										<div class="embed-responsive embed-responsive-16by9">
											<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/126087353?badge=0" allowfullscreen></iframe>
										</div>
									</div>	

									<div class="hz_top_post">

										<div class="hz_title_and_meta">
											<div class="hz_title"><h3><a href="#">Marketing Tactics to Drive Online Sales Marketing Tactics to Drive Online Sales</a></h3></div>
											<div class="hz_meta_post">
												<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
												<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Design</a></span>
												<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
												<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
											</div>
										</div>
									</div>

									<div class="hz_main_post_content">
										<div class="hz_content">
											<p>
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
												Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
												when an unknown printer took a galley of type and scrambled it to make a type 
												specimen book. It has survived not only five centuries, but also the leap into 
												electronic typesetting, remaining essentially unchanged. It was popularised in 
												the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, 
												and more recently with desktop publishing software like Aldus PageMaker including 
												versions of Lorem Ipsum. Contrary to popular belief, Lorem Ipsum is not simply random text. 
												It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. 
												Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, 
												looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum 
												passage, and going through the cites of the word in classical literature, 
												discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 
												and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) 
												by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very 
												popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", 
												comes from a line in section 1.10.32.
												Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has 
												been the industry's standard dummy text ever since the 1500s, when an unknown printer took a 
												galley of type and scrambled it to make a type specimen book. It has survived not only five 
												centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It 
												was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
												and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
												Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of 
												classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a 
												Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure 
												Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word 
												in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 
												1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero,
												written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. 
												The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
											</p>
										</div>

										<div class="hz_bottom_post">
											<div class="hz_tags">
												<span><a href="#">Design</a></span>
												<span><a href="#">photoshop</a></span>
												<span><a href="#">HTML</a></span>
											</div>

											<div class="hz_icon_shere">
			                                    <span class="share_toggle pi-btn">
			                                        <i class="fa fa-share-alt"></i>
			                                    </span>
			                                    <div class="hz_share">
													<span><a href="#"><i class="fa fa-facebook"></i></a></span>
													<span><a href="#"><i class="fa fa-twitter"></i></a></span>
													<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
													<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
												</div>
											</div>
										</div>
									</div>
								</div>
									
								<div class="user_info_post"><!--Start user info post -->
									<div class="post_title"><h4>About the Author</h4></div>
									<div class="user_pic">
										<img src="assets/imgdemo/pic.jpg" alt="Ashmawi Sami">
									</div>
									<div class="min_user_info">
										<h4>Ashmawi Sami</h4>
										<p>written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. 
										The first line of Lorem Ipsum, Written In 45 BC. This Book Is A Treatise On The Theory Of Ethics, Very Popular During The Renai Of Ethics, Very Popular During The Renaissance...</p>
									</div>
									<div class="social_icon">
										<span><a href="#"><i class="fa fa-facebook"></i></a></span>
										<span><a href="#"><i class="fa fa-twitter"></i></a></span>
										<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
										<span><a href="#"><i class="fa fa-youtube"></i></a></span>
										<span><a href="#"><i class="fa fa-dribbble"></i></a></span>
										<span><a href="#"><i class="fa fa-behance"></i></a></span>
										<span><a href="#"><i class="fa fa-instagram"></i></a></span>
										<span><a href="#"><i class="fa fa-vimeo-square"></i></a></span>
										<span><a href="#"><i class="fa fa-linkedin"></i></a></span>
									</div>
								</div><!--End user info post -->
									
								<div class="single_related_posts"><!-- Start Related Posts -->
									<div class="post_title"><h4>Related Posts</h4></div>

									<ul id="post_related_block" class="post_block">
										
										<li class="item">
											<div class="img_box">
												<a href="#">
												<img src="assets/imgdemo/slid-wid/3.png" alt="FASHIONABLE SWEATERS FOR EVERYONE">
												</a>
											</div>
											<h5><a href="#">FASHIONABLE SWEATERS FOR EVERYONE</a></h5>
											<span class="date">Januray 13, 2015</span>
										</li>

										<li class="item">
											<div class="img_box">
												<a href="#">
												<img src="assets/imgdemo/slid-wid/1.png" alt="FASHIONABLE SWEATERS FOR EVERYONE">
												</a>
											</div>
											<h5><a href="#">FASHIONABLE SWEATERS FOR EVERYONE</a></h5>
											<span class="date">Januray 13, 2015</span>
										</li>

										<li class="item">
											<div class="img_box">
												<a href="#">
												<img src="assets/imgdemo/slid-wid/4.png" alt="FASHIONABLE SWEATERS FOR EVERYONE">
												</a>
											</div>
											<h5><a href="#">FASHIONABLE SWEATERS FOR EVERYONE</a></h5>
											<span class="date">Januray 13, 2015</span>
										</li>


										<li class="item">
											<div class="img_box">
												<a href="#">
												<img src="assets/imgdemo/slid-wid/2.png" alt="FASHIONABLE SWEATERS FOR EVERYONE">
												</a>
											</div>
											<h5><a href="#">FASHIONABLE SWEATERS FOR EVERYONE</a></h5>
											<span class="date">Januray 13, 2015</span>
										</li>

										<li class="item">
											<div class="img_box">
												<a href="#">
												<img src="assets/imgdemo/slid-wid/3.png" alt="FASHIONABLE SWEATERS FOR EVERYONE">
												</a>
											</div>
											<h5><a href="#">FASHIONABLE SWEATERS FOR EVERYONE</a></h5>
											<span class="date">Januray 13, 2015</span>
										</li>


										<li class="item">
											<div class="img_box">
												<a href="#">
												<img src="assets/imgdemo/slid-wid/4.png" alt="FASHIONABLE SWEATERS FOR EVERYONE">
												</a>
											</div>
											<h5><a href="#">FASHIONABLE SWEATERS FOR EVERYONE</a></h5>
											<span class="date">Januray 13, 2015</span>
										</li>
									</ul>
								</div><!-- End Related Posts -->
									

								<div class="conmments_block"><!--Start Conmments Block -->
									<div class="post_title"><h4>Comments</h4></div>

									<ol class="commentlist clearfix">
									    <li class="comment">
									        <div class="comment-body clearfix"> 
									            <div class="avatar"><img alt="" src="assets/imgdemo/pic.jpg"></div>
									            <div class="comment-text">
									                <div class="author clearfix">
									                	<div class="comment-meta">
									                        <span><a href="#">ashmawi</a></span>
									                        <div class="date">August 20 , 2014 at 10:00 pm</div> 
									                    </div>
									                    <a class="comment-reply button" href="#">Reply</a>
									                </div>
									                <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat.</p>
									                </div>
									            </div>
									        </div>
									        <ul class="children">
									            <li class="comment">
									                <div class="comment-body clearfix"> 
									                	<div class="avatar"><img alt="" src="assets/imgdemo/pic.jpg"></div>
									                    <div class="comment-text">
									                        <div class="author clearfix">
									                            <div class="comment-meta">
									                                <span>sami</span>
									                                <div class="date">August 20 , 2014 at 10:00 pm</div> 
									                            </div>
									                            <a class="comment-reply button" href="#">Reply</a>
									                        </div>
									                        <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat.</p>
									                        </div>
									                    </div>
									                </div>
									                <ul class="children">
									                    <li class="comment">
									                        <div class="comment-body clearfix"> 
									                            <div class="avatar"><img alt="" src="assets/imgdemo/pic.jpg"></div>
									                            <div class="comment-text">
									                                <div class="author clearfix">
									                                    <div class="comment-meta">
									                                        <span>ahmed</span>
									                                        <div class="date">August 20 , 2014 at 10:00 pm</div> 
									                                    </div>
									                                    <a class="comment-reply button" href="#">Reply</a>
									                                </div>
									                                <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat.</p>
									                                </div>
									                            </div>
									                        </div>
									                    </li>
									                 </ul><!-- End children -->
									            </li>
									            <li class="comment">
									            	<div class="comment-body clearfix"> 
								                        <div class="avatar"><img alt="" src="assets/imgdemo/pic.jpg"></div>
								                        <div class="comment-text">
								                            <div class="author clearfix">
								                                <div class="comment-meta">
								                                    <span>fawzy</span>
								                                    <div class="date">August 20 , 2014 at 10:00 pm</div> 
								                                </div>
								                                <a class="comment-reply button" href="#">Reply</a>
								                            </div>
								                            <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat.</p>
								                            </div>
								                        </div>
								                    </div>
									            </li>
									        </ul><!-- End children -->
									    </li>
									    <li class="comment">
									        <div class="comment-body clearfix"> 
									            <div class="avatar"><img alt="" src="assets/imgdemo/pic.jpg"></div>
									            <div class="comment-text">
									                <div class="author clearfix">
									                	<div class="comment-meta">
									                        <span>john</span>
									                        <div class="date">August 20 , 2014 at 10:00 pm</div> 
									                    </div>
									                    <a class="comment-reply button" href="#">Reply</a>
									                </div>
									                <div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat.</p>
									                </div>
									            </div>
									        </div>
									    </li>
									</ol>
								</div><!--End Conmments Block -->
									

									
								<div id="respond"><!-- Start respond Conmments -->
		                            <div class="reply-title">
		                                <h4 class="post_title">Leaver your comment</h4>
		                            </div><!-- COMMENT FORM -->
		                            
		                           
										<div class="comment-form">
											<form method="post" class="form-js" action="#">
												<div class="form-input">
													<i class="fa fa-user"></i>
													<input type="text" name="name" id="name" placeholder="Your Name">
												</div>
												<div class="form-input">
													<i class="fa fa-envelope"></i>
													<input type="email" name="mail" id="mail" placeholder="Email">
												</div>
												<div class="form-input">
													<i class="fa fa-home"></i>
													<input type="url" name="url" id="url" placeholder="URL">
												</div>
												<div class="form-input">
													<i class="fa fa-comment"></i>
													<textarea placeholder="Message" name="message" id="message"></textarea>
												</div>
												<input type="submit" class="button" value="Send Message"></form>
										</div>

		                            <!-- END / COMMENT FORM -->
		                        </div>
									<!-- End respond Conmments -->
							</article><!-- End article -->
									
						</div>	
					</div><!--End Posts Areaa -->

					<div class="sidebar col-md-4"><!--Start Sidebar -->
						<div class="row"><!-- Start Row -->
							<div class="inner_sidebar"><!-- Start inner sidebar -->

								<div class="widget  widget_about_me"><!-- Start widget About Me -->
									<div class="about_me">
										<div class="my_pic">
											<img src="../../../ww17.hamzh.info/demo/hworih/wp-content/uploads/2015/05/photodune-11116609-beautiful-small-kid-model-with1.html" alt="Ashmawi Sami">
										</div>
										<div class="my_name">
											<h4>Ashmawi Sami</h4>
										</div>
										<div class="my_words">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing
											 elit, sed do eiusmod tempor incididunt ut labore et
											  dolore magna aliqua. Ut enim ad minim veniam</p>
										</div>
										<div class="social_icon">
											<span><a href="#"><i class="fa fa-facebook"></i></a></span>
											<span><a href="#"><i class="fa fa-twitter"></i></a></span>
											<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
											<span><a href="#"><i class="fa fa-youtube"></i></a></span>
											<span><a href="#"><i class="fa fa-dribbble"></i></a></span>
											<span><a href="#"><i class="fa fa-behance"></i></a></span>
											<span><a href="#"><i class="fa fa-instagram"></i></a></span>
											<span><a href="#"><i class="fa fa-vimeo-square"></i></a></span>
											<span><a href="#"><i class="fa fa-skype"></i></a></span>
											<span><a href="#"><i class="fa fa-linkedin"></i></a></span>
										</div>
									</div>
								</div><!-- End widget About Me -->

								<div class="widget widget_search"><!-- Start widget search -->
									<h4 class="widget_title">Search</h4>
									<form>
										<input type="search" value="Search here ..." onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
										<input class="button" type="submit" value="Search Now">
									</form>
								</div><!-- End widget search -->

								<div class="widget widget_recent_post"><!-- Start widget recent post -->
									<h4 class="widget_title">Recent Post</h4>
									<ul class="recent_post">

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/1.png" height="80" width="80" alt="Appropriately simplify quality imperatives"></a>
											</figure>
											<div class="widget_post_info">
												<h5><a href="#">Appropriately simplify quality imperatives</a></h5>
												<div class="post_meta">
													<span><a href="#"><i class="fa fa-comments-o"></i> 0 comments</a></span>
													<span class="date_meta"><a href="#"><i class="fa fa-calendar"></i> Mar 10, 2015</a></span>
												</div>
											</div>
										</li>

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/2.png" height="80" width="80" alt="Appropriately simplify quality imperatives"></a>
											</figure>
											<div class="widget_post_info">
												<h5><a href="#">Appropriately simplify quality imperatives</a></h5>
												<div class="post_meta">
													<span><a href="#"><i class="fa fa-comments-o"></i> 0 comments</a></span>
													<span class="date_meta"><a href="#"><i class="fa fa-calendar"></i> Mar 10, 2015</a></span>
												</div>
											</div>
										</li>

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/3.png" height="80" width="80" alt="Appropriately simplify quality imperatives"></a>
											</figure>
											<div class="widget_post_info">
												<h5><a href="#">Appropriately simplify quality imperatives</a></h5>
												<div class="post_meta">
													<span><a href="#"><i class="fa fa-comments-o"></i> 0 comments</a></span>
													<span class="date_meta"><a href="#"><i class="fa fa-calendar"></i> Mar 10, 2015</a></span>
												</div>
											</div>
										</li>

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/4.png" height="80" width="80" alt="Appropriately simplify quality imperatives"></a>
											</figure>
											<div class="widget_post_info">
												<h5><a href="#">Appropriately simplify quality imperatives</a></h5>
												<div class="post_meta">
													<span><a href="#"><i class="fa fa-comments-o"></i> 0 comments</a></span>
													<span class="date_meta"><a href="#"><i class="fa fa-calendar"></i> Mar 10, 2015</a></span>
												</div>
											</div>
										</li>
									</ul>
								</div><!-- End widget recent post -->

								<div class="widget widget_categories"><!-- Start widget categories -->
									<h4 class="widget_title">Categories</h4>
									<ul class="categories">
										<li><a href="#">Music <span>(5)</span></a><i class="fa fa-angle-double-right"></i></li>
										<li><a href="#">Fashion <span>(10)</span></a><i class="fa fa-angle-double-right"></i></li>
										<li><a href="#">Media <span>(5)</span></a><i class="fa fa-angle-double-right"></i></li>
										<li><a href="#">Medical <span>(10)</span></a><i class="fa fa-angle-double-right"></i></li>
										<li><a href="#">Sports <span>(5)</span></a><i class="fa fa-angle-double-right"></i></li>
										<li><a href="#">Internet <span>(10)</span></a><i class="fa fa-angle-double-right"></i></li>
										<li><a href="#">Technology <span>(6)</span></a><i class="fa fa-angle-double-right"></i></li>
									</ul>
								</div><!-- End widget categories -->

								<div class="widget hamzh_flickr"><!-- Start widget search -->
									<h4 class="widget_title">Flickr Photos</h4>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/1.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/2.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/3.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>


									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/4.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/5.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/6.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/7.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/8.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/9.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/10.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/11.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>

									<div class="flickr_badge_image">
										<a href="#">
											<img src="assets/imgdemo/flickr/12.png" alt="A photo on Flickr" title="Halloween 2014 at Envato in Melbourne">
										</a>
									</div>
								</div><!-- End widget search -->

								<div class="widget widget_slider_dribbble"><!-- Start widget recent entries -->
									<h4 class="widget_title">Dribbble Gallery</h4>
									<ul class="slid_widget_dribbble">

									</ul>
								</div><!-- End Widget Recent Entries -->

								<div class="widget widget_recent_comments"><!--Start widget recent comments -->
									<h4 class="widget_title">Recent comments</h4>
									<ul class="recent_post">

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/5.png" height="80" width="80" alt="Rapidiously coordinate cross-unit communities without"></a>
											</figure>
											<div class="widget_post_info">
												<strong class="comment-author"><a href="#"><i class="fa fa-user"></i> Ashmawi Sami</a></strong>
												<span class="comment-c"><a href="#">Rapidiously coordinate cross-unit communities without </a></span>
											</div>
										</li>

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/5.png" height="80" width="80" alt="Rapidiously coordinate cross-unit communities without"></a>
											</figure>
											<div class="widget_post_info">
												<strong class="comment-author"><a href="#"><i class="fa fa-user"></i> Ashmawi Sami</a></strong>
												<span class="comment-c"><a href="#">Rapidiously coordinate cross-unit communities without </a></span>
											</div>
										</li>

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/5.png" height="80" width="80" alt="Rapidiously coordinate cross-unit communities without"></a>
											</figure>
											<div class="widget_post_info">
												<strong class="comment-author"><a href="#"><i class="fa fa-user"></i> Ashmawi Sami</a></strong>
												<span class="comment-c"><a href="#">Rapidiously coordinate cross-unit communities without </a></span>
											</div>
										</li>

										<li>
											<figure class="widget_post_thumbnail">
												<a href="#"><img src="assets/imgdemo/wid/5.png" height="80" width="80" alt="Rapidiously coordinate cross-unit communities without"></a>
											</figure>
											<div class="widget_post_info">
												<strong class="comment-author"><a href="#"><i class="fa fa-user"></i> Ashmawi Sami</a></strong>
												<span class="comment-c"><a href="#">Rapidiously coordinate cross-unit communities without </a></span>
											</div>
										</li>

									</ul>
								</div><!--End widget recent comments -->

								<div class="widget widget_advertisement"><!-- Start widget Advertisement -->
									<h4 class="widget_title">Advertisement</h4>
									<div class="slid_widget">
										<div class="ads_w"><a href="#"><img src="assets/imgads-300x250.png" alt="Advertisement"></a></div>
									</div>
								</div><!-- End Widget Advertisement -->

								<div class="widget widget_twitter"><!-- Start Widget Latest Tweets -->
									<h4 class="widget_title">Latest Tweets</h4>
									<ul class="tweet_list">
										<li class="tweet_first tweet_odd">
											<span class="tweet_text">RT <span class="at">@</span>
												<a href="#">EnvatoStudio</a>
												How My Crappy Day Jobs Made Me a High-Earning Freelancer:
												<a href="#">studioblog.envato.com/high-earning-f…</a>
												<a href="#">pic.twitter.com/u9utuNSQEU</a>
											</span> <br>
											<span class="tweet_time">
												<a href="#" title="view tweet on twitter">about 30 minutes ago</a>
											</span>
										</li>
										<li class="tweet_even">
											<span class="tweet_text">RT <span class="at">@</span>
												<a href="#">EnvatoMarket</a>
												 Some awesome entries in our flag contest! How would you redesign the flag for Planet Earth? <a href="#">enva.to/UbB-C</a>
												 <a href="#">pic.twitter.com/XMLD6jmnkQ</a>
											</span> <br>
											<span class="tweet_time">
												<a href="#" title="view tweet on twitter">about an hour ago</a>
											</span>
										</li>
									</ul>
								</div><!-- End Widget Latest Tweets -->

								<div class="widget widget_login"><!-- Start widget widget login -->
									<h4 class="widget_title">Login</h4>
									<div class="widget_login">
										<form>
											<input type="text" value="Username" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
											<div class="widget_login_password">
												<input type="password" value="Password" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
											</div>
											<input type="submit" value="Login" class="button">
										</form>
									</div>
								</div><!-- End Widget widget login -->

								<div class="widget  widget_tag_cloud"><!-- Start widget tag cloud -->
									<h4 class="widget_title">Tags</h4>
									<div class="tagcloud">
										<a href="#">audio</a>
										<a href="#">dailymotion</a>
										<a href="#">Gallery</a>
										<a href="#">LightBox</a>
										<a href="#">Link</a>
										<a href="#">mp3</a>
										<a href="#">nature</a>
										<a href="#">post</a>
										<a href="#">Quote</a>
										<a href="#">slider</a>
										<a href="#">soundcloud</a>
										<a href="#">sport</a>
										<a href="#">Standard</a>
										<a href="#">Twitter</a>
										<a href="#">vimeo</a>
									</div>
								</div><!-- End widget tag cloud -->

							</div><!-- End inner sidebar -->
						</div><!-- End Row -->
					</div><!--End Sidebar -->
				</div><!-- End posts sidebar -->
			</div><!-- End main content -->
