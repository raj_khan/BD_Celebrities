

			<div class="main_content container">
				<div class="mian_slider clearfix"><!--Start Mian slider -->
					<div class="big_silder col-md-8"><!-- Start big silder -->
						<div class="row">
							<ul id="big-slid-post" class="a-post-box"><!-- Start big-slid-post -->
								<li>
									<div class="feat-item img-section" data-bg-img="assets/img/demo/slid/1.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="single.php" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/img/demo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>

								<li>
									<div class="feat-item img-section" data-bg-img="assets/img/demo/slid/2.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="single.php" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/img/demo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>

								<li>
									<div class="feat-item img-section" data-bg-img="assets/img/demo/slid/3.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="single.php" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/img/demo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>

								<li>
									<div class="feat-item img-section" data-bg-img="assets/img/demo/slid/4.png">
									<div class="latest-overlay"></div>
										<div class="latest-txt">
											<span class="latest-cat"><a href="#">Design</a></span>
											<h3 class="latest-title"><a data-dummy="fdfgdfgfg" href="single.php" rel="bookmark" title="#">There are many of Lorem Ipsum available</a></h3>
											<div class="big-latest-content">
												<p>Words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary,</p>
											</div>

											<div class="hz_admin_pic"> 
												<img src="assets/img/demo/pic.jpg" class="img-responsive" alt="Responsive image">
											</div>
											<span class="hz_post_by"><i class="fa fa-user"></i><a href="#">Ashmawi Sami</a></span>
											<span class="latest-meta">
												<span class="latest-date"><i class="fa fa-clock-o"></i> Mar 10, 2015</span>
											</span>
										</div>
									</div>
								</li>
							</ul><!-- End big-slid-post -->
						</div>
					</div><!-- End big silder -->
					
					<div class="post_box col-md-4"><!--Start Post box -->
						<div class="row">
							<article class="a-post-box">
									<figure class="latest-img"><img src="assets/img/demo/slid/2.png" alt="If you are going to use a passage of Lorem Ipsum" class="latest-cover"></figure>
									<div class="latest-overlay"></div>
									<div class="latest-txt">
										<h4 class="latest-title"><a data-dummy="dfsdfsdfdsf" href="single.php" rel="bookmark" title="If you are going to use a passage of Lorem Ipsum">If you are going to use a passage of Lorem Ipsum</a></h4>
										<span class="latest-cat"><a href="#">Life</a></span>
										<span class="latest-meta">
											<span class="latest-date"><i class="fa fa-clock-o"></i>26-5-2014</span>
										</span>
									</div>
					    	</article>

							<article class="a-post-box">
									<figure class="latest-img"><img src="assets/img/demo/slid/4.png" alt="going to use a passage of Lorem" class="latest-cover"></figure>
									<div class="latest-overlay"></div>
									<div class="latest-txt">
										<h4 class="latest-title"><a data-dummy="dfsdfsdfdsf" href="single.php" rel="bookmark" title="going to use a passage of Lorem">going to use a passage of Lorem</a></h4>
										<span class="latest-cat"><a href="#">Life</a></span>
										<span class="latest-meta">
											<span class="latest-date"><i class="fa fa-clock-o"></i>26-5-2014</span>
										</span>
									</div>
					    	</article>
						</div>
					</div><!--End Post box -->
				</div><!--End Mian slider -->

				<div class="posts_sidebar clearfix">
					<!--Start Posts Areaa main_grid_layout -->
					<div class="posts_areaa col-md-12 main_grid_layout clearfix">
						<div class="row">
							<!-- grid_layout -->
							<div class="grid_layout">
								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/10.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/15.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- //hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/14.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/17.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/18.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<!-- <article class="hz_post grid_post clearfix">
									<div class="video_post">

										<div class="hz_thumb_post">
											
											<div class="icon_post_format">
												<i class="fa fa-video-camera"></i>
											</div>

											<div class="embed-responsive embed-responsive-16by9">
												<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/126087353?badge=0" allowfullscreen></iframe>
											</div>
										</div>	

										<div class="hz_top_post">
											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Design</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article> -->
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/13.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<!-- <article class="hz_post grid_post clearfix">
									<div class="video_post">
										<div class="hz_thumb_post">

											<div class="icon_post_format">
												<i class="fa fa-video-camera"></i>
											</div>

											<div class="embed-responsive embed-responsive-16by9">
											  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/w06D4wBcJcs" allowfullscreen></iframe>
											</div>
										</div>

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Sales Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Design</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article> -->
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="lightbox_post">
										<div class="hz_thumb_post">
											<div class="icon_post_format">
												<i class="fa fa-search-plus"></i>
											</div>
											<a class="image_link" href="assets/img/demo/post/16.png" data-lightbox="lightbox_id" data-title="Click the right half of the image to move forward.">
											<img class="lightbox_image" src="assets/img/demo/post/8.png" alt=""/>
											<div class="overlay"></div></a>
										</div>	

										<div class="hz_top_post">
											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">LightBox</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="standard_post">
										<div class="hz_thumb_post">
											<a href="#">

												<div class="icon_post_format">
													<i class="fa fa-image"></i>
												</div>

												<img src="assets/img/demo/post/9.png" class="img-responsive" alt="Responsive image">
											</a>
										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Marketing Tactics to Drive Online Sales</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Image</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Ut enim ad minim veniam, ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->

								<!-- hz_post grid_post -->
								<article class="hz_post grid_post clearfix">
									<div class="slider_post">
										<div class="hz_thumb_post">

											<div class="icon_post_format">
												<i class="fa fa-image"></i>
											</div>
											
											<div class="post_silder">
												<ul id="post-slid-post">
													<li><div class="item"><a href="#"><img src="assets/img/demo/slid/1.png" alt="Mirror Edge"></a></div></li>
													<li><div class="item"><a href="#"><img src="assets/img/demo/slid/2.png" alt="Mirror Edge"></a></div></li>
													<li><div class="item"><a href="#"><img src="assets/img/demo/slid/3.png" alt="Mirror Edge"></a></div></li>
													<li><div class="item"><a href="#"><img src="assets/img/demo/slid/4.png" alt="Mirror Edge"></a></div></li>
												</ul>
											</div>

										</div>	

										<div class="hz_top_post">

											<div class="hz_title_and_meta">
												<a href="single.php">
													<h4 class="hz_title">Drive Online Sales Marketing</h4>
												</a>
												<div class="hz_meta_post">
													<span class="hz_cat_post"><i class="fa fa-folder-open"></i><a href="#">Design</a></span>
													<span class="hz_date_post"><i class="fa fa-calendar"></i><a href="#">Mar 10, 2015</a></span>
													<span class="hz_date_post"><i class="fa fa-comments"></i><a href="#">Comments</a></span>
												</div>
											</div>
										</div>

										<div class="hz_main_post_content">
											<div class="hz_content">
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat...</p>
											</div>

											<div class="hz_bottom_post">

												<div class="hz_read_more">
													<a href="#">Read More</a>
												</div>

												<div class="hz_icon_shere">

				                                    <span class="share_toggle pi-btn">
				                                        <i class="fa fa-share-alt"></i>
				                                    </span>

				                                    <div class="hz_share">
														<span><a href="#"><i class="fa fa-facebook"></i></a></span>
														<span><a href="#"><i class="fa fa-twitter"></i></a></span>
														<span><a href="#"><i class="fa fa-google-plus"></i></a></span>
														<span><a href="#"><i class="fa fa-stumbleupon"></i></a></span>
													</div>

												</div>

											</div>
										</div>
									</div>
								</article>
								<!-- // hz_post grid_post -->


							</div>
							<!-- grid_layout -->

							<nav class="pagination_post"><!-- pagination_post -->
							  <ul class="pager">
							    <li><a href="#">Previous</a></li>
							    <li><a href="#">Next</a></li>
							  </ul>
							</nav><!-- // pagination_post -->
						</div>
					</div>
					<!--End Posts Areaa main_grid_layout -->
				</div><!-- posts_sidebar -->
			</div><!-- main_content -->