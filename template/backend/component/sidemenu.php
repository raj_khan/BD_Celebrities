
<!-- ==========================  SideBar ================-->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header text-center" style="color: #9c6411;">MAIN NAVIGATION</li>

            <!--======Main Slider========-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-image"></i> <span>Main Slider</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('add-main-slider'); ?>"><i class="fa fa-edit"></i>Add Main Slider</a></li>

                    <li><a href="<?php echo base_url('view-main-slider'); ?>"><i class="fa fa-eye"></i>Manage Main Slider </a></li>

                </ul>
            </li>


            <!--====== Category========-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sitemap"></i> <span> Category</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(); ?>Category/add_category"><i class="fa fa-edit"></i>Add Main Category</a></li>

                    <li><a href="<?php echo base_url('view-all-category'); ?>"><i class="fa fa-eye"></i>Manage Category </a></li>
                    <li><a href="<?php echo base_url('add-celebrity-category'); ?>"><i class="fa fa-edit"></i>Add Celebrity Category</a></li>

                    <li><a href="<?php echo base_url('view-celebrity-category'); ?>"><i class="fa fa-eye"></i>Manage Celebrity Category </a></li>

                </ul>
            </li>


            <!--====== Cricketers bio========-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-creative-commons"></i> <span> Cricketers</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('create-cricketers-bio'); ?>"><i class="fa fa-edit"></i>Add Cricketers Bio</a></li>
                    <li><a href="<?php echo base_url('view-cricketers-bio'); ?>"><i class="fa fa-eye"></i>Manage Cricketers Bio </a></li>

                </ul>
            </li>


            <!--====== Actress bio========-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-adn"></i> <span> Actress</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('create-actress-bio'); ?>"><i class="fa fa-edit"></i>Add Actress Bio</a></li>
                    <li><a href="<?php echo base_url('view-actress-bio'); ?>"><i class="fa fa-eye"></i>Manage Actress Bio </a></li>
                </ul>
            </li>


            <!--====== Singers bio========-->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-music"></i> <span> Singers</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url('create-singers-bio'); ?>"><i class="fa fa-edit"></i>Add Singers Bio</a></li>
                    <li><a href="<?php echo base_url('view-singers-bio'); ?>"><i class="fa fa-eye"></i>Manage Singers Bio </a></li>
                </ul>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
